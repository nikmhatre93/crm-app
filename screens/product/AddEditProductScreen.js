import React from 'react'
import { View, TextInput, Alert, Text, KeyboardAvoidingView, ToastAndroid } from 'react-native'
import { findCallLimit } from '../../redux/constant'
import { base_url } from '../../base_url'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { dispatchProducts, productsForDropdown } from '../../redux/actions/productActions'
import styles from '../common/stylesheet'
import SubmitButton from '../common/SubmitButton'
import axios from 'axios'
import { actions } from '../../redux/actions/actions'
import { titleCase } from '../common/commonFunctions'

var setTimeoutId = null

class AddEditProductScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            name: '',
            gst: '',
            headerText: 'Add Product',
            id: '',
            error: false,
            is_exists: false,
            old_name: ''
        }
    }

    // Set header
    static navigationOptions = ({ navigation }) => {
        let val = navigation.getParam('id') ? 'Update Product' : 'New Product'
        return {
            headerTitle: <Header name={val} />,
        };
    };


    componentDidMount = async () => {
        const { navigation } = this.props;
        const id = navigation.getParam('id');
        const isSearch = navigation.getParam('isSearch')

        // if id found then update else create
        if (id) {
            productsFromState = isSearch ? this.props.searchProducts : this.props.products
            product = productsFromState[id]
            if (!product) {
                res = await axios.get(`${base_url}/products/${id}`,
                    { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                product = res.data
            }

            this.setState({
                name: titleCase(product.name),
                gst: product.GST.toString(),
                headerText: "Update Product",
                id,
                old_name: product.name
            })
        }
    }

    // Toast message
    _toast = (val, redirect) => {

        ToastAndroid.show(
            val,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
        );

        if (redirect) {
            this.props.navigation.goBack()
        }

    }

    // Create product
    _addData = () => {
        let _gst = parseFloat(this.state.gst)
        let state = {
            name: this.state.name.trim().toLowerCase(),
            GST: isNaN(_gst) ? '' : _gst,
        }

        let allow = true
        if (state.name.length == 0 || state.GST.length == 0) {
            allow = false
        }

        if (allow) {
            axios.post(`${base_url}/products`, state,
                { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this.props.dispatchProducts(res.data, actions.add_product)
                    this.props.productsForDropdown(this.props.login.jwt)

                    this._toast('Product has been added successfully.', true)
                })
                .catch(err => {
                    Alert.alert(
                        'Information',
                        `Product with name '${this.state.name}' already exists.`,
                        [
                            {
                                text: 'Ok',
                            },
                        ]
                    )
                })

        } else {
            this.setState({ error: !allow })
        }
    }

    // Update product
    _updateData = () => {
        let state = {
            name: this.state.name.trim().toLowerCase(),
            GST: parseFloat(this.state.gst),
        }

        let allow = true
        if (state.name.length == 0 || state.GST.length == 0) {
            allow = false
        }

        if (allow) {
            axios.put(`${base_url}/products/${this.state.id}`, state,
                { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }).then(res => {
                    this.props.dispatchProducts(res.data, actions.update_product_list)
                    this.props.productsForDropdown(this.props.login.jwt)
                    this._toast('Product has been updated successfully.', true)
                })
                .catch(err => {
                    Alert.alert(
                        'Information',
                        `Product with name '${this.state.name}' already exists.`,
                        [
                            {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                            },
                        ]
                    )
                })
        } else {
            this.setState({ error: !allow })
        }
    }

    _onTextChange = (text) => {
        this.setState({ name: text })


        if (setTimeoutId != null) {
            clearInterval(setTimeoutId)
        }

        setTimeoutId = setTimeout(() => {
            this.is_exists(this.state.name)
        }, 500)

    }


    // Check product with given name exits or not
    is_exists = (text) => {

        let url = `${base_url}/products?name=${text.trim().toLowerCase()}&_limit=${findCallLimit}`
        let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }

        axios.get(url, headers)
            .then(res => {
                let _old = this.state.old_name
                let _new = this.state.name

                if (this.state.id == '') {
                    this._conditionsForNameCheck(res)
                } else {
                    if (_old.trim().toLowerCase() == _new.trim().toLowerCase()) {
                        this.setState({ is_exists: false })

                    } else {
                        this._conditionsForNameCheck(res)
                    }
                }
            })
            .catch(err => {
                this.setState({ is_exists: false })
            })
    }

    _conditionsForNameCheck = (res) => {
        if (res.data.length > 0) {
            this.setState({ is_exists: true })
        } else {
            this.setState({ is_exists: false })
        }
    }

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={{ padding: 10, ...styles.screenBackground }}>
                {/* <Header name={this.state.headerText} /> */}

                {
                    this.state.error &&
                    <Text style={{ color: 'red', textAlign: 'center' }}>
                        All the fields are mandatory.
                    </Text>
                }

                {/* <View style={{ flex: 10 }}> */}

                {
                    this.state.is_exists &&
                    <Text style={{ color: 'red', textAlign: 'center' }}>
                        Product name already exists.
                    </Text>
                }

                <View style={{ position: 'relative' }}>
                    <Text style={styles.labelStyle}>Product Name</Text>
                    <TextInput placeholder="Product Name"
                        style={styles.formStyle}
                        value={this.state.name}
                        onChangeText={(text) => this._onTextChange(text)} />
                </View>


                <View style={{ position: 'relative' }}>
                    <Text style={styles.labelStyle}>GST (%)</Text>
                    <TextInput placeholder="GST (%)"
                        style={styles.formStyle}
                        keyboardType='numeric'
                        value={this.state.gst}
                        onChangeText={(text) => this.setState({ gst: text })} />
                </View>

                {/* </View> */}

                <SubmitButton
                    headerText={this.state.headerText}
                    _apiCall={this.state.id === '' ? this._addData : this._updateData} />
            </KeyboardAvoidingView>
        )
    }
}


const mapDispatchToProps = {
    dispatchProducts,
    productsForDropdown
}

const mapStateToProps = (state) => {
    return {
        products: state.products,
        searchProducts: state.searchProducts,
        login: state.login
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddEditProductScreen)