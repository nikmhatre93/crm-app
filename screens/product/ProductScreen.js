import React from 'react'
import ManagementScreen from '../common/ManagementScreen'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { getProducts, ProductSearchList } from '../../redux/actions/productActions'
import { LogoutButton } from '../AuthScreen'

// Headers for product screen
const headerItem = {
    name: 'Name',
    price: 'Price',
    GST: 'GST(%)'
}

// Act as base screen for product
class ProductScreen extends React.Component {

    componentDidMount = () => {
        this.props.navigation.setParams({
            name: this.props.login.user.username
        })
    }

    static navigationOptions = ({ navigation }) => {
        const name = navigation.state.params ? navigation.state.params.name : ''
        return {
            headerTitle: <Header name='Products' icon={true} />,
            headerRight: <LogoutButton navigation={navigation} name={name} />
        }
    };


    render() {
        return <ManagementScreen
            buttonText="Add a Product"
            type="product"
            filterUrl='filter_products'
            searchData={this.props.searchProducts}
            data={this.props.products}
            screen="AddEditProduct"
            headerItem={headerItem}
            navigation={this.props.navigation}
            getPaginateData={this.props.getProducts}
            searchFunc={this.props.ProductSearchList}
        />
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.products,
        searchProducts: state.searchProducts,
        login: state.login
    }
}

mapDispatchToProps = {
    getProducts,
    ProductSearchList
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductScreen);