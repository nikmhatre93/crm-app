import React from 'react'
import { View, Text } from 'react-native'
import styles from '../common/stylesheet'

// Act as header for few files or modal pop-ups
export default Customer = (props) => (
    <View style={[styles.boxView, { height: 38, margin: 10 }]}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', fontSize: 17 }}>{props.customer.shop_name}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', fontSize: 17 }}>{props.customer.owner_name}</Text>
        </View>
    </View>
)