import React from 'react'
import { Button, View, TouchableOpacity, Text, BackHandler } from 'react-native'
import { base_url } from '../../base_url'
import axios from 'axios'
import TableView from '../common/TableView'
// import { headerItem } from '../order/OrderScreen'
import { getStatus } from '../common/getBadgeStatus'
import Header from '../common/Header'
import History from './History'
import { btnColor } from '../common/stylesheet'
import Customer from './Customer'
import ViewOrdersModal from '../order/ViewOrder'
// import console = require('console');


/*
    This screen shows customer's order history.
*/

export default class CustomerOrdersHistory extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            id: '',
            data: [],
            orderData: [],
            orderModalVisible: false,
            currentOrder: {}
        }
    }

    static navigationOptions = {
        headerTitle: <Header name='Order History' />,
    };

    _navigate = (item) => {
        this.props.navigation.popToTop()
        this.props.navigation.push('AddEditOrder',
            { id: item._id, isSearch: false, from: 'CustomerOrdersHistory' }
        )
    }

    _generateList = (item, header) => {

        let re_create = header ? item.recreate : <TouchableOpacity
            style={{
                marginHorizontal: 10,
                borderColor: btnColor,
                paddingVertical: 3,
                borderWidth: 0.5,
                borderRadius: 2,
                elevation: 2,
                backgroundColor: btnColor,

            }}
            onPress={() => this._navigate(item)}>
            <Text style={{ textAlign: 'center', color: 'white' }}>Create</Text>
        </TouchableOpacity>

        return <TouchableOpacity
            onPress={() => this._toggleOrderModal(true, item)}>
            <TableView
                col1={item.order_id}
                col2={item.total.toString()}
                colStatus={getStatus(item.status)}
                colClickable={re_create}
                isHeader={header}
            />
        </TouchableOpacity>

    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
    }

    _handleBackPress = () => {
        this.props.navigation.pop()
    }

    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);

        const { navigation } = this.props;
        const id = navigation.getParam('id');
        const token = navigation.getParam('token');
        const user = navigation.getParam('user');

        let url = `${base_url}/ordersByCustomer`
        let headers = { headers: { 'Authorization': `Bearer ${token}` } }
        let data = { customer: id, num: 0, user: user._id }

        /* removed from data to get all orders (In order history) */
        // role: user.role.type, 

        axios.post(url, data, headers)
            .then(res => this.setState({ data: res.data, id }))
    }


    _toggleOrderModal = (val, item) => {
        if (item) {
            const { navigation } = this.props;
            const token = navigation.getParam('token');

            axios.get(`${base_url}/subordersByOrderId/${item._id}`,
                { headers: { 'Authorization': `Bearer ${token}` } })
                .then(res => {
                    this.setState({
                        orderModalVisible: val,
                        currentOrder: item,
                        orderData: res.data
                    })
                })
        } else {
            this.setState({ orderModalVisible: val })
        }
    }

    render() {
        const { navigation } = this.props;
        const customer = navigation.getParam('customer');

        // console.log(this.props)

        let headerItem = {
            order_id: 'Order Id',
            total: 'Balance',
            status: 'Status',
            recreate: 'Create',
        }

        return (
            <View style={{ flex: 1 }}>

                <ViewOrdersModal
                    orderModalVisible={this.state.orderModalVisible}
                    orderData={this.state.orderData}
                    _toggleOrderModal={this._toggleOrderModal}
                    currentOrder={this.state.currentOrder} />

                <Customer customer={customer} />

                <View style={{ marginHorizontal: 10, marginVertical: 5, }}>

                    <Button
                        onPress={() => {
                            this.props.navigation.popToTop()
                            this.props.navigation.navigate('makePayments', {
                                customer
                            })
                        }
                        }
                        color={btnColor}
                        title="Make a Payment"
                    />

                </View>


                {/* <View> */}
                <History
                    _generateList={this._generateList}
                    data={this.state.data}
                    headerItem={headerItem}
                />

            </View>
        )


    }
}
