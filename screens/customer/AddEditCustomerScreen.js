import React from 'react'
import { View, TextInput, Text, TouchableOpacity, AsyncStorage, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import { base_url } from '../../base_url'
import { dispatchCustomers, customersForDropdown } from '../../redux/actions/customerActions'
import Header from '../common/Header'
import AreaList from '../common/AreaList'
import styles from '../common/stylesheet'
import SubmitButton from '../common/SubmitButton'
import axios from 'axios'
import { actions } from '../../redux/actions/actions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

class AddUpdateCustomerScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            owner_name: '',
            shop_name: '',
            gst_id: '',
            area: '',
            phone: '',
            mobile: '',
            fssai: '',
            headerText: 'Add Customer',
            id: '',
            temp_customer: {},
            error: false,
            gstError: false,
            mobileError: false
        }
    }

    // Set header
    static navigationOptions = ({ navigation }) => {
        let val = navigation.getParam('id') ? 'Update Customer' : 'New Customer'
        return {
            headerTitle: <Header name={val} />,
        };
    };

    componentDidMount = async () => {
        const { navigation } = this.props;
        const id = navigation.getParam('id');
        const isSearch = navigation.getParam('isSearch')


        // if id found then update customer else add customer
        if (id) {
            customersFromState = isSearch ? this.props.searchCustomers : this.props.customers
            customer = customersFromState[id]

            if (!customer) {
                res = await axios.get(`${base_url}/customers/${id}`,
                    { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                customer = res.data
            }

            this.setState({
                owner_name: customer.owner_name,
                shop_name: customer.shop_name,
                gst_id: customer.gst_id,
                area: customer.area._id,
                headerText: "Update Customer",
                id,
                temp_customer: customer,
                phone: customer.phone,
                mobile: customer.mobile,
                fssai: customer.fssai
            })
        } else {
            let area = await AsyncStorage.getItem('globalArea')
            if (!area) {
                area = ''
            }
            this.setState({ area })
        }
    }


    // Show toast message
    _toast = (val, redirect) => {

        ToastAndroid.show(
            val,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
        );

        if (redirect) {
            this.props.navigation.goBack()
        }

    }

    // Generation post data for create and update
    _prepareData = () => {
        let state = { ...this.state }
        let gstError = state.gst_id == '' ? true : this._regexTest(state.gst_id)
        let mobileError = state.mobile.length == 0 || state.mobile.length == 10 ? false : true
        let allow = true
        if (state.area == '' || state.shop_name.length == 0 || !gstError || mobileError) {
            allow = false
        }

        return { state, allow, gstError, mobileError }
    }

    // Create customer
    _addData = () => {
        let d = this._prepareData()

        if (d.allow) {
            let url = `${base_url}/customers`
            let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }

            axios.post(url, { ...d.state }, headers)
                .then(res => {
                    let data = res.data
                    this.props.dispatchCustomers(data, actions.add_customer)
                    this.props.customersForDropdown(this.props.login.jwt)
                    this._toast('Customer has been added successfully.', true)

                })
                .catch(error => this._toast('Something went wrong, Please try again later.', false))
        } else {
            if (!d.gstError) {
                this.setState({ gstError: true })
            } else if (d.mobileError) {
                this.setState({ mobileError: true })
            } else {
                this.setState({ error: !d.allow })
            }
        }

    }

    // Update customer
    _updateData = () => {
        let d = this._prepareData()

        if (d.allow) {
            let url = `${base_url}/customers/${d.state.id}`
            let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }

            axios.put(url, { ...d.state }, headers)
                .then(res => {
                    let data = res.data
                    this.props.dispatchCustomers(data, actions.update_customers_list)
                    this.props.customersForDropdown(this.props.login.jwt)

                    this._toast('Customer has been updated successfully.', true)
                })
                .catch(error => this._toast('Something went wrong, Please try again later.', false))

        } else {
            if (!d.gstError) {
                this.setState({ gstError: true })
            } else if (d.mobileError) {
                this.setState({ mobileError: true })
            } else {
                this.setState({ error: !d.allow })
            }
        }
    }

    // Check if gst id is valid or not
    _regexTest = (text) => {
        let regEx = new RegExp('^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$')
        return regEx.test(text)
    }

    _onEndEditingGst = (e) => {
        let err = e.nativeEvent.text == '' ? true : this._regexTest(e.nativeEvent.text)
        if (err) {
            this.setState({ gstError: false })
        } else {
            this.setState({ gstError: true })
        }
    }

    _onEndEditingMobile = (e) => {
        let textLength = e.nativeEvent.text.length
        let err = textLength == 0 || textLength == 10 ? false : true
        if (!err) {
            this.setState({ mobileError: false })
        } else {
            this.setState({ mobileError: true })
        }
    }

    _onAreaChange = (itemValue) => {
        this.setState({ area: itemValue })
    }

    render() {
        const { navigation } = this.props;
        const id = navigation.getParam('id');


        return (
            <KeyboardAwareScrollView
                keyboardShouldPersistTaps="always"
            >
                <View style={{ padding: 10, ...styles.screenBackground }}>

                    {this.state.error && <Text style={{ color: 'red', textAlign: 'center' }}>
                        Area & Shop Name  are mandatory
                    </Text>}

                    {/* <View style={{ flex: 10 }}> */}

                    <AreaList
                        data={this.props.areas}
                        value={this.state.area}
                        func={this._onAreaChange}
                    />


                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>Shop Name</Text>
                        <TextInput placeholder="Shop Name"
                            style={{ ...styles.formStyle }}
                            value={this.state.shop_name}
                            onChangeText={(text) => this.setState({ shop_name: text })} />
                    </View>


                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>Owner Name</Text>
                        <TextInput placeholder="Owner Name"
                            style={{ ...styles.formStyle }}
                            value={this.state.owner_name}
                            onChangeText={(text) => this.setState({ owner_name: text })} />
                    </View>


                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>GSTIN</Text>
                        <TextInput placeholder="GSTIN"
                            style={{ ...styles.formStyle }}
                            onEndEditing={this._onEndEditingGst}
                            value={this.state.gst_id}
                            onChangeText={(text) => this.setState({ gst_id: text })} />
                    </View>

                    {
                        this.state.gstError &&
                        <Text style={{ color: 'red', textAlign: 'center' }}>
                            Please enter valid GST-IN Id
                        </Text>
                    }

                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>Phone</Text>
                        <TextInput placeholder="Phone"
                            style={{ ...styles.formStyle }}
                            value={this.state.phone}
                            keyboardType='numeric'
                            onChangeText={(text) => this.setState({ phone: text })} />
                    </View>


                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>Mobile</Text>
                        <TextInput placeholder="Mobile"
                            style={{ ...styles.formStyle }}
                            value={this.state.mobile}
                            keyboardType='numeric'
                            onEndEditing={this._onEndEditingMobile}
                            onChangeText={(text) => this.setState({ mobile: text })} />
                    </View>

                    {
                        this.state.mobileError &&
                        <Text style={{ color: 'red', textAlign: 'center' }}>
                            Please enter valid mobile number.
                        </Text>
                    }


                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>FSSAI</Text>
                        <TextInput placeholder="FSSAI"
                            style={{ ...styles.formStyle }}
                            value={this.state.fssai}
                            onChangeText={(text) => this.setState({ fssai: text })} />
                    </View>

                    {/* </View> */}

                    <SubmitButton
                        headerText={this.state.headerText}
                        _apiCall={this.state.id === '' ? this._addData : this._updateData} />


                    {
                        id &&
                        <History
                            navigation={this.props.navigation}
                            id={id}
                            customer={this.state.temp_customer}
                            login={this.props.login}
                        />
                    }
                </View>
            </KeyboardAwareScrollView>
        )
    }
}


const History = (props) => {
    return <View style={{ flexDirection: "row", marginTop: 30 }}>
        <View style={{ padding: 10, flex: 1 }}>
            <TouchableOpacity
                style={[styles.boxView, { padding: 8, justifyContent: 'center' }]}
                onPress={
                    () => props.navigation.navigate('customerOrdersHistory', {
                        id: props.id,
                        customer: props.customer,
                        token: props.login.jwt,
                        user: props.login.user

                    })
                }>
                <Text style={{ textAlign: 'center' }}>
                    Order History
            </Text>
            </TouchableOpacity>
        </View>

        <View style={{ padding: 10, flex: 1 }}>
            <TouchableOpacity
                style={[styles.boxView, { padding: 8, justifyContent: 'center' }]}

                onPress={
                    () => props.navigation.navigate('customerPaymentHistory', {
                        id: props.id,
                        customer: props.customer,
                        token: props.login.jwt,
                        user: props.login.user
                    })
                }>
                <Text style={{ textAlign: 'center' }}>
                    Payment History
            </Text>
            </TouchableOpacity>

        </View>
    </View>
}


const mapDispatchToProps = {
    dispatchCustomers,
    customersForDropdown
}

const mapStateToProps = (state) => {
    return {
        areas: state.areas,
        customers: state.customers,
        login: state.login,
        searchCustomers: state.searchCustomers
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUpdateCustomerScreen);