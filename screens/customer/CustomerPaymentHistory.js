import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import { base_url } from '../../base_url'
import axios from 'axios'
import TableView from '../common/TableView'
import Header from '../common/Header'
import moment from 'moment'
import History from './History'
import Customer from './Customer'
import { connect } from 'react-redux'
import ViewPaymentModal from '../payment/ViewPayment'

// Header for current screen
const headerItem = {
    data: {
        "cheque/cash": 'Cheque/Cash',
        total_paid: 'Amount',
        createdAt: 'Date'
    }
}

/*
    Current screen shows Payment history for customer.
*/

class CustomerPaymentHistory extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            id: '',
            data: [],
            paymentModalVisible: false,
            customer: {},
            paymentData: [],
            user: {}
        }
    }

    // Header for current screen
    static navigationOptions = {
        headerTitle: <Header name='Payment History' />,
    };

    // get the data and show it on model
    _togglePaymentModal = (val, id) => {
        if (id) {
            axios.get(`${base_url}/subOrderByPayment/${id}`,
                { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this.setState({
                        paymentData: res.data.data,
                        customer: res.data.customer,
                        user: res.data.user,
                        paymentModalVisible: val
                    })
                })
        } else {
            this.setState({ paymentModalVisible: val })
        }
    }

    _generateList = (item, header) => {
        item = item.data
        let txt = <TableView
            col1={item.createdAt == "Date" ? "Payment Date" : moment(item.createdAt).format('ll')}
            col3={item.total_paid}
            col2={item['cheque/cash'] == 'cs' ? 'Cash' : 'Cheque'}
            isHeader={header}
        />

        if (item['cheque/cash'] == 'Cheque/Cash') {
            return txt
        } else {
            return (
                <TouchableOpacity
                    onPress={() => this._togglePaymentModal(true, item._id)}>
                    {txt}
                </TouchableOpacity>
            )
        }

    }

    componentDidMount = () => {
        const { navigation } = this.props;
        const id = navigation.getParam('id');
        const token = navigation.getParam('token');
        const user = navigation.getParam('user')
        let url = `${base_url}/paymentsByCustomer`
        let headers = { headers: { 'Authorization': `Bearer ${token}` } }
        let data = { customer: id, user: user._id }

        axios.post(url, data, headers)
            .then(res => {
                this.setState({ data: res.data, id })
            })
    }

    render() {
        const { navigation } = this.props;
        const customer = navigation.getParam('customer');

        return (
            <View style={{ flex: 1 }}>
                <ViewPaymentModal
                    paymentModalVisible={this.state.paymentModalVisible}
                    _togglePaymentModal={this._togglePaymentModal}
                    paymentData={this.state.paymentData}
                    customer={this.state.customer} />


                <Customer customer={customer} />



                <History
                    customer={customer}
                    _generateList={this._generateList}
                    data={this.state.data}
                    headerItem={headerItem}
                />

            </View>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        login: state.login
    }
}

export default connect(mapStateToProps, null)(CustomerPaymentHistory)