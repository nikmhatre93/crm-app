import React from 'react'
import { View, FlatList } from 'react-native'
import NoRecordTemplate from '../common/NoRecordTemplate'

export default History = (props) => (
    <View style={{ flex: 1 }}>
        {
            props.data.length > 0
                ? <View style={{ flex: 1 }}>
                    <FlatList
                        style={{ margin: 10, flex: 1 }}
                        ListHeaderComponent={props._generateList(props.headerItem, true)}
                        keyExtractor={(item) => item._id}
                        data={props.data}
                        renderItem={({ item }) => props._generateList(item, false)}
                        stickyHeaderIndices={[0]} />
                </View>
                : <NoRecordTemplate />
        }
    </View>
)