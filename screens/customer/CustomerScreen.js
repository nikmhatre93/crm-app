import React from 'react'
import ManagementScreen from '../common/ManagementScreen'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { getCustomers, CustomerSearchList } from '../../redux/actions/customerActions'
import { LogoutButton } from '../AuthScreen'

// header for customer screen
const headerItem = {
    shop_name: 'Shop',
    owner_name: 'Owner',
    area: { name: 'Area' },
    gst_id: 'GSTIN'
}

// Base customer screen
class CustomerScreen extends React.Component {

    componentDidMount = () => {
        this.props.navigation.setParams({
            name: this.props.login.user.username
        })
    }

    static navigationOptions = ({ navigation }) => {
        const name = navigation.state.params ? navigation.state.params.name : ''
        return {
            headerTitle: <Header name='Customers' icon={true} />,
            headerRight: <LogoutButton navigation={navigation} name={name} />
        }

    };

    render() {
        return <ManagementScreen
            buttonText="Add a Customer"
            type="customer"
            filterUrl='filter_customers'
            data={this.props.customers}
            searchData={this.props.searchCustomers}
            screen="AddEditCustomer"
            navigation={this.props.navigation}
            headerItem={headerItem}
            getPaginateData={this.props.getCustomers}
            searchFunc={this.props.CustomerSearchList}
        />
    }
}

const mapStateToProps = (state) => {
    return {
        customers: state.customers,
        searchCustomers: state.searchCustomers,
        login: state.login
    }
}

mapDispatchToProps = {
    getCustomers,
    CustomerSearchList
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomerScreen);