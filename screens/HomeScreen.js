// Not in use


// ------------------------------------------------------------------

// import React, { Component } from 'react'
// import { View, Image, Text, TouchableOpacity } from 'react-native'
// import { connect } from 'react-redux'
// import { getAreas } from '../redux/actions/areaActions'
// import { getCustomers } from '../redux/actions/customerActions'
// import { getOrders } from '../redux/actions/ordersActions'
// import { getProducts } from '../redux/actions/productActions'
// import Product from '../img/printer-cartridges.png'
// import Customer from '../img/networking.png'
// import Payment from '../img/money.png'
// import Order from '../img/shopping-cart-23.png'
// import Header from './common/Header'


// let list1 = [
//     { name: 'Customers', fun: 'manageCustomers', icon: Customer },
//     { name: 'Products', fun: 'manageProducts', icon: Product }
// ]

// let list2 = [
//     { name: 'Payments', fun: 'managePayment', icon: Payment },
//     { name: 'Orders', fun: 'manageOrders', icon: Order }
// ]

// class HomeScreen extends Component {
//     static navigationOptions = {
//         headerTitle: <Header name="Dashboard" />
//     };

//     _generateCards = (list) => {



//         return list.map(item => (
//             <TouchableOpacity
//                 onPress={() => this.props.navigation.navigate(item.fun)}
//                 key={item.name}
//                 style={{
//                     backgroundColor: 'white',
//                     flex: 1,
//                     elevation: 5,
//                     marginTop: 20,
//                     marginRight: 10,
//                     marginBottom: 20,
//                     marginLeft: 10,
//                     padding: 10,
//                 }}>
//                 <View style={{ flex: 3, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
//                     <Image source={item.icon} style={{ width: 125, height: 125 }} />
//                 </View>
//                 <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
//                     <Text>{item.name}</Text>
//                 </View>


//             </TouchableOpacity>

//         ))

//     }

//     render() {
//         return (
//             <View style={{ flex: 1, flexDirection: 'column', backgroundColor: 'white' }}>

//                 <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly' }}>
//                     {this._generateCards(list1)}
//                 </View>

//                 <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-evenly' }}>
//                     {this._generateCards(list2)}
//                 </View>


//             </View>
//         );
//     }
// }

// const mapDispatchToProps = {
//     getProducts,
//     getCustomers,
//     getOrders,
//     getAreas
// }

// const mapStateToProps = (state) => {
//     return {
//         loggedInUser: state.login,
//     }
// }

// export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);