import React from 'react'
import {
    View,
    Text,
    TouchableOpacity,
    AsyncStorage,
    Alert,
    ToastAndroid,
    BackHandler,
    TextInput,
    ScrollView,
} from 'react-native'
import { connect } from 'react-redux'
import { base_url } from '../../base_url'
import Header from '../common/Header'
import SubOrderScreen from './SubOrderScreen'

import { dispatchOrders } from '../../redux/actions/ordersActions'
import CustomerList from '../common/CustomerList'
import AreaList from '../common/AreaList'
import SubmitButton from '../common/SubmitButton'
import axios from 'axios'
import lodash from 'lodash'
import { actions } from '../../redux/actions/actions'
import { Badge } from '../common/TableView'
import { getStatus, badgeStyle } from '../common/getBadgeStatus'
import styles from '../common/stylesheet'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { NavigationEvents } from 'react-navigation';

// import SectionedMultiSelect from 'react-native-sectioned-multi-select';
// import Icon3 from 'react-native-vector-icons/MaterialIcons';

// import Spinner from 'react-native-loading-spinner-overlay';

export const defaultSubTotal = "Total"
let total = 0

let stop_call = false

class AddUpdateOrderScreen extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            area: "",
            customer: '',
            headerText: 'Place an order',
            id: '',
            subOrders: {
                1: {
                    quantity: '',
                    product: '',
                    subtotal: defaultSubTotal,
                    key: 1,
                    price: "",
                    options: false,
                    min_price: 0,
                    min_price_error: false,
                    selectedIndex: 0
                },
                2: {
                    quantity: '',
                    product: '',
                    subtotal: defaultSubTotal,
                    key: 2,
                    price: "",
                    options: false,
                    min_price: 0,
                    min_price_error: false,
                    selectedIndex: 0
                }
            },
            status: { style: { backGroundColor: {}, textStyle: {} }, val: 'Fetching..', showButton: true },
            deleteList: [],
            error: false,
            lastKey: 2,
            lastIndex: 0,
            amount_to_be_collected: '',
            // multi_select_list: [],
            loader: false,
            remark: '',
            validation: true

        }
    }

    // Set headers
    static navigationOptions = ({ navigation }) => {
        let val = navigation.getParam('from') == 'ManagementScreen'
            ? navigation.getParam('id')
                ? 'Update Order' : 'New Order' : 'New Order'
        const _handleBackPress = navigation.getParam('_handleBackPress')
        return {
            headerTitle: <Header name={val} />,
            headerLeft: _handleBackPress && <TouchableOpacity
                onPress={_handleBackPress}
            >
                <MaterialIcons
                    name="arrow-back"
                    size={25}
                    color="black"
                    style={{ marginLeft: 15 }}
                />
            </TouchableOpacity>
        };
    };

    componentDidMount = async () => {

        this.props.navigation.setParams({
            _handleBackPress: this._handleBackPress
        })

        const { navigation } = this.props;
        const id = navigation.getParam('id')
        const isSearch = navigation.getParam('isSearch')
        const from = navigation.getParam('from')

        // if id found then update else create
        if (id) {

            ordersFromState = isSearch ? this.props.searchOrders : this.props.orders
            order = ordersFromState[id]
            let status = ''

            if (!order) {
                res = await axios.get(`${base_url}/orders/${id}`,
                    { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                order = res.data
            }

            status = getStatus(order.status)

            axios.get(`${base_url}/subordersByOrderId/${id}`,
                { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(response => {

                    let res = response.data
                    let subOrders = {}
                    let tempTotal = 0
                    let lastKey = 1
                    let lastIndex = 0
                    // let multi_select_list = []

                    res.forEach((item, index) => {


                        let tempPrice = parseFloat(item.subtotal) / parseFloat(item.quantity)
                        let price = isNaN(tempPrice) ? "" : tempPrice

                        lastKey += 1
                        tempTotal += parseFloat(item.subtotal)
                        // multi_select_list[index] = item.product

                        let selectedIndex = this.props.products[item.product].index
                        if (selectedIndex > lastIndex) {
                            lastIndex = selectedIndex
                        }

                        subOrders[index + 1] = {
                            quantity: item.quantity,
                            product: item.product,
                            subtotal: item.subtotal == null ? 0 : item.subtotal,
                            price: price,
                            key: index + 1,
                            _id: item._id,
                            options: false,
                            min_price: this.props.products[item.product].min_price,
                            min_price_error: false,
                            selectedIndex
                        }
                    })


                    let backGroundColor = {}
                    let textStyle = {}
                    let BadgeStyle = badgeStyle(status)
                    backGroundColor = BadgeStyle.backGroundColor
                    textStyle = BadgeStyle.textStyle
                    let headerText = ''
                    let showButton = true
                    if (from == 'ManagementScreen') {
                        headerText = "Update an order"
                        switch (status) {
                            case 'INIT':
                                showButton = true
                                break
                            case 'PNDG':
                                showButton = true
                                break
                            case 'CMPLT':
                                showButton = false
                                break
                            case 'PRTL':
                                showButton = false
                                break
                            default:
                                showButton = true
                                break
                        }
                    } else {
                        headerText = "Place An Order"
                    }

                    this.setState({
                        customer: order.customer._id,
                        headerText: headerText,
                        id,
                        subOrders,
                        // multi_select_list,
                        status: { style: { backGroundColor, textStyle }, val: status, showButton },
                        lastKey,
                        amount_to_be_collected: order.amount_to_be_collected == null
                            || order.amount_to_be_collected == ""
                            ? ' ' : order.amount_to_be_collected,
                        remark: order.remark,
                        lastIndex,
                        // validation: false
                    })

                    if (status == 'INIT' || status == 'PNDG') {
                        this._addTab()
                    }


                    total = tempTotal


                    setTimeout(() => this._validation(true), 0)
                })

        } else {
            let area = await AsyncStorage.getItem('globalArea')
            if (!area) {
                area = ''
            }
            this.setState({ area })

        }
    }


    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
    }


    _handleBackPress = () => {
        let msg = 'Remove all changes?'

        // if (this.state.id != '') {
        //     msg = 'Are you sure want go back? Any changes that have been made will not be saved.'
        // }

        let suborder = { ...this.state.subOrders[1] }
        if (suborder.quantity != ''
            || suborder.product != ''
            || suborder.price != '') {
            Alert.alert(
                'Attention',
                msg,
                [
                    {
                        text: 'No',
                        onPress: () => console.log('Cancel Pressed'),
                        style: 'cancel',
                    },
                    { text: 'Yes', onPress: () => this.props.navigation.goBack() },
                ],
                { cancelable: false },
            )
        } else {
            this.props.navigation.goBack()
        }

        return true
    }

    // Shows quantity options each time quantity text box is in focus
    _showOptions = (key, val) => {

        let subOrders = { ...this.state.subOrders }
        if (subOrders[key]) {
            subOrders[key].options = val
        }
        this.setState({ subOrders })
    }

    // After product selection
    _setProduct = (key, value, product, index) => {

        // console.log(ieym)
        // let multi_select_list = [...this.state.multi_select_list]

        let subOrders = { ...this.state.subOrders }
        subOrders[key].product = value
        let cal = 0
        if (subOrders[key].quantity !== '' && subOrders[key].price !== '') {
            cal = parseFloat(subOrders[key].price) * parseFloat(subOrders[key].quantity)
            let total = value === ''
                ? defaultSubTotal
                : cal
            subOrders[key].subtotal = total

        }


        // if (multi_select_list.indexOf(value[0]) == -1) {
        //     multi_select_list[key - 1] = value[0]
        // }

        subOrders[key].selectedIndex = index

        let _i = this.props.products[value] ? this.props.products[value].index : 0

        if (product) {
            subOrders[key]['min_price'] = product.min_price
        }

        this.setState({ subOrders, lastIndex: _i })

        this._min_price_error(key)
        this._dynamic_addTab()
    }

    // After price entered
    _setProductPrice = (key, value) => {
        let subOrders = { ...this.state.subOrders }
        subOrders[key].price = isNaN(value) ? '' : value
        let cal = 0
        if (subOrders[key].quantity !== '' && subOrders[key].product !== '') {
            cal = parseFloat(value) * parseFloat(subOrders[key].quantity)
            let total = isNaN(cal)
                ? defaultSubTotal
                : cal
            subOrders[key].subtotal = total
        }

        this.setState({ subOrders })
    }


    // After quantity register or selected
    _setQuntity = (key, value) => {

        let subOrders = { ...this.state.subOrders }
        subOrders[key].quantity = isNaN(value) ? '' : value
        let cal = 0
        if (subOrders[key].price !== '' && subOrders[key].product !== '') {
            cal = parseFloat(subOrders[key].price) * parseFloat(value)

            let total = isNaN(cal)
                ? defaultSubTotal
                : cal
            subOrders[key].subtotal = total
        }

        this.setState({ subOrders })
    }


    // Generate post data
    _createDataForAddAndUpdate = (state) => {
        let allow = true
        let status = '00'
        delete state.subOrders[this.state.lastKey]

        Object.keys(state.subOrders).forEach(item => {

            if (state.subOrders[item].key != this.state.lastKey) {
                let temp_price = state.subOrders[item].price
                if (isNaN(temp_price)) {
                    temp_price = temp_price.trim()
                }
                if (temp_price == '' || state.subOrders[item].quantity == '' || state.subOrders[item].quantity == 0) {
                    status = '01'
                }

                let price_validation = false

                if (this.state.validation) {

                    let _p = parseFloat(state.subOrders[item].price)
                    let min_price = parseFloat(state.subOrders[item].min_price)

                    price_validation = isNaN(_p) || (_p < min_price)

                }


                if (state.subOrders[item].product == '' || price_validation) {

                    allow = false

                }

            }

        })

        state['status'] = status

        return { state, allow }
    }


    // Create Order
    _addData = () => {

        this.setState({ loader: true })
        let state;
        let allow;

        temp = this._createDataForAddAndUpdate({ ...this.state })
        state = temp.state
        allow = temp.allow

        let data = {
            ...state,
            user: this.props.login.user._id,
            total
        }

        if (allow && !stop_call) {
            stop_call = true
            axios.post(`${base_url}/orders/createWithSubOrder`, data, { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    stop_call = false
                    let data = { ...res.data, paymentList: { data: [] } }
                    this.props.dispatchOrders(data, actions.add_order)

                    this._toast('Order has been placed successfully.', true)
                    this.setState({ loader: false })
                })
                .catch(err => {
                    stop_call = false
                    this._toast('Something went wrong, Please try again later.', false)
                    this.setState({ loader: false })
                })
        } else {
            state.subOrders[this.state.lastKey] = {
                quantity: '',
                product: '',
                price: '',
                subtotal: defaultSubTotal,
                key: this.state.lastKey,
                options: false,
                min_price: '0',
                min_price_error: false
            }
            this.setState({ error: !allow, subOrders: state.subOrders, loader: false })
        }

    }

    // toast message
    _toast = (val, redirect) => {

        ToastAndroid.show(
            val,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
        );

        if (redirect) {
            this.props.navigation.goBack()
        }
    }

    // Update order
    _updateData = () => {

        let state;
        let allow;

        let deleteList = [...new Set(this.state.deleteList)]

        if (deleteList.length > 0) {
            axios.post(`${base_url}/suborders/deleteSubOrders`, { delete: deleteList }, { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
        }


        temp = this._createDataForAddAndUpdate({ ...this.state })
        state = temp.state
        allow = temp.allow

        let data = {
            ...state,
            user: this.props.login.user._id,
            total
        }

        if (allow) {
            Alert.alert(
                'Information',
                'Receive print notification?',
                [
                    {
                        text: 'No',
                        onPress: () => this._updateApiCall(data, false),
                        style: 'cancel',
                    },
                    {
                        text: 'Yes',
                        onPress: () => this._updateApiCall(data, true)
                    },
                ],
                { cancelable: false },
            )
        } else {
            this.setState({ error: !allow })
        }

    }

    // Show pop-up with yes or no options
    // if yes fire socket event from backend to make it appear in print panel
    _updateApiCall = (data, noti) => {
        this.setState({ loader: true })
        let url = `${base_url}/orders/updateWithOrder/${this.state.id}`
        let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }
        data.noti = noti

        axios.put(url, data, headers)
            .then(res => {

                let data = { ...res.data[0] }
                this.props.dispatchOrders(data, actions.update_orders_list)
                this._toast('Order has been updated successfully.', true)
                this.setState({ loader: false })
            })
            .catch(err => {

                this._toast('Something went wrong, Please try again later.', false)
                this.setState({ loader: false })
            })

    }

    // Add a tab for product (SubOrder)
    _addTab = () => {
        let subOrders = { ...this.state.subOrders }
        let allKeys = Object.keys(subOrders).map(i => parseInt(i))
        let lastKey = lodash.max(allKeys) + 1
        subOrders[lastKey] = {
            quantity: '',
            product: '',
            price: '',
            subtotal: defaultSubTotal,
            key: lastKey,
            options: false,
            min_price: '0',
            min_price_error: false,
            selectedIndex: 0,
        }

        this.setState({
            subOrders,
            lastKey
        })
    }

    _dynamic_addTab = () => {
        let subOrders = { ...this.state.subOrders }
        let allKeys = Object.keys(subOrders).map(i => parseInt(i))
        let maxKey = lodash.max(allKeys)

        if (subOrders[maxKey] && subOrders[maxKey].product != '') {
            this._addTab()
        }
    }

    // Remove tab
    _removeTab = (key, _id) => {
        let deleteList = []
        if (_id) {
            deleteList = [...this.state.deleteList]
            deleteList.push(_id)
        }

        let subOrders = { ...this.state.subOrders }


        // let multi_select_list = this.state.multi_select_list.filter((item) => item != subOrders[key].product)
        delete subOrders[key]

        this.setState({
            subOrders,
            deleteList,
            // multi_select_list
        })
    }

    _onCustomerChange = (itemValue) => {
        this.setState({ customer: itemValue })
    }

    _onAreaChange = (itemValue) => {
        this.setState({ area: itemValue, customer: '' })
    }

    // price entered should be equal or more than min price
    _min_price_error = (key) => {
        if (this.state.validation) {

            let subOrders = { ...this.state.subOrders }

            if (subOrders[key].price != "" && parseFloat(subOrders[key].min_price) > parseFloat(subOrders[key].price)) {
                subOrders[key].min_price_error = true

            } else {
                subOrders[key].min_price_error = false
            }
            this.setState({ subOrders })

        }
    }


    // _onConfirm = () => {

    //     let subOrders = {}
    //     let lastKey = 1
    //     this.state.multi_select_list.forEach((_id, index) => {
    //         key = index + 1
    //         lastKey = key

    //         if (this.state.subOrders[key]) {
    //             subOrders[key] = {
    //                 ...this.state.subOrders[key],
    //                 product: `${_id}`
    //             }
    //         } else {
    //             subOrders[key] = {
    //                 quantity: '',
    //                 product: `${_id}`,
    //                 subtotal: defaultSubTotal,
    //                 key: key,
    //                 price: "",
    //                 options: false,
    //                 min_price: '0',
    //                 min_price_error: false
    //             }
    //         }

    //     })

    //     lastKey += 1
    //     subOrders[lastKey] = {
    //         quantity: '',
    //         product: '',
    //         subtotal: defaultSubTotal,
    //         key: lastKey,
    //         price: "",
    //         options: false,
    //         min_price: '0',
    //         min_price_error: false
    //     }

    //     this.setState({ subOrders, lastKey })
    // }

    _onWillFocus = () => {
        BackHandler.addEventListener('hardwareBackPress', this._handleBackPress);
    }

    _onWillBlur = () => {
        BackHandler.removeEventListener('hardwareBackPress', this._handleBackPress);
    }


    // check min price validation
    _validation = (val) => {
        // const val = !this.state.validation

        let subOrders = { ...this.state.subOrders }

        Object.keys(subOrders).forEach(key => {

            if (key != this.state.lastKey) {
                if (val) {
                    // force price validation
                    let min_price_error = false
                    let _p = parseFloat(subOrders[key].price)
                    let min_price = parseFloat(subOrders[key].min_price)

                    if (isNaN(_p) || _p < min_price) {
                        min_price_error = true
                    }

                    subOrders[key] = { ...subOrders[key], min_price_error }


                } else {
                    // if no price validation
                    if (subOrders[key].min_price_error) {
                        subOrders[key] = { ...subOrders[key], min_price_error: false }
                    }
                }
            }
        })


        this.setState({ validation: val, subOrders })
    }

    render() {
        total = 0
        // console.log(this.props.customers)
        Object.keys(this.state.subOrders).forEach(key => {

            try {
                if (isNaN(parseFloat(this.state.subOrders[key].subtotal))) {
                    total += 0
                } else {
                    total += parseFloat(this.state.subOrders[key].subtotal)
                }

            } catch (err) {
                total += 0
            }
        })

        const { navigation } = this.props;
        const id = navigation.getParam('id')
        const from = navigation.getParam('from')

        return (
            <View style={{ flex: 1, padding: 10, ...styles.screenBackground }}>

                <NavigationEvents
                    onWillFocus={this._onWillFocus}
                    onWillBlur={this._onWillBlur}
                />

                {
                    this.state.error &&
                    <Text style={{ color: 'red', textAlign: 'center' }}>
                        All the fields are mandatory.
                    </Text>
                }

                {/* <ScrollView> */}
                <View style={{ flexDirection: "row", margin: 3 }}>

                    <TouchableOpacity
                        style={[styles.formStyle, {
                            margin: 0,
                            paddingHorizontal: 4,
                            paddingVertical: 2,
                            height: 30
                        },]}
                        onPress={() => this._validation(!this.state.validation)}
                    >
                        <Text
                            style={{
                                color: this.state.validation ? 'green' : 'red',
                                textAlign: 'center',
                                fontSize: 16,
                                // textDecorationLine: 'underline'
                            }}
                        >
                            {this.state.validation ? 'On' : 'Off'}
                        </Text>
                    </TouchableOpacity>

                    {
                        !isNaN(total) && total != 0 ?
                            <View style={{ flex: 3 }}>
                                <Text
                                    style={{ textAlign: 'center', fontWeight: 'bold', marginVertical: 5 }}>
                                    Order Total: {parseFloat(total).toFixed(2)}
                                </Text>

                            </View> : null
                    }

                    {

                        id && from == 'ManagementScreen' ?
                            <View style={{ flex: 1, alignItems: 'flex-end' }}>
                                <Badge
                                    val={this.state.status.val}
                                    backGroundColor={this.state.status.style.backGroundColor}
                                    textStyle={this.state.status.style.textStyle}
                                />
                            </View> : null
                    }

                </View>

                <ScrollView style={{ flex: 10 }}
                    keyboardShouldPersistTaps="always"
                >


                    {
                        !this.state.id &&
                        <AreaList
                            data={this.props.areas}
                            value={this.state.area}
                            func={this._onAreaChange}
                            guide={true}
                            is_selected={this.state.area == "" ? false : true}
                        />
                    }


                    {
                        this.state.area !== '' || this.state.id ?
                            <View style={{ flexDirection: 'row' }}>

                                <CustomerList
                                    style={{ flex: 1 }}
                                    id={id}
                                    data={this.props.customers}
                                    value={this.state.customer}
                                    func={this._onCustomerChange}
                                    area={this.state.area}
                                    guide={true}
                                    is_selected={this.state.customer == "" ? false : true}
                                />

                                {
                                    this.state.customer !== '' &&
                                    <TouchableOpacity
                                        onPress={
                                            () => this.props.navigation.navigate('customerOrdersHistory', {
                                                id: this.state.customer,
                                                customer: this.props.customers[this.state.customer],
                                                token: this.props.login.jwt,
                                                user: this.props.login.user

                                            })
                                        }
                                        style={[styles.formStyle, { marginHorizontal: 2, padding: 5 }]}
                                    >
                                        <MaterialIcons
                                            name="history"
                                            size={30}
                                        />
                                    </TouchableOpacity>
                                }

                            </View>
                            : null
                    }


                    {
                        this.state.customer !== '' &&
                        <View style={{ flex: 1 }}>

                            <SubOrderScreen
                                data={this.state.subOrders}
                                lastKey={this.state.lastKey}
                                lastIndex={this.state.lastIndex}
                                _setQuntity={this._setQuntity}
                                _setProduct={this._setProduct}
                                _setProductPrice={this._setProductPrice}
                                _removeTab={this._removeTab}
                                _showOptions={this._showOptions}
                                _min_price_error={this._min_price_error}
                                _min_price_validation={this.state.validation}
                            />
                        </View>
                    }


                    {
                        this.state.customer !== '' &&
                        <View>

                            {/* <SectionedMultiSelect
                                styles={{ ...dropdownStyle, }}
                                items={lodash.values(this.props.products)}
                                uniqueKey='_id'
                                searchPlaceholderText='Search products...'
                                showDropDowns={true}
                                showCancelButton={false}
                                hideConfirm={false}
                                showChips={false}
                                hideSearch={false}
                                confirmText="Done"
                                onSelectedItemsChange={(item) => {
                                    this.setState({ multi_select_list: item })
                                }}
                                selectText="Select Multiple Products"
                                onConfirm={this._onConfirm}
                                modalAnimationType="slide"
                                selectedIconComponent={<Icon3 name="check-circle" size={17} style={{ color: "green" }} />}
                                selectedItems={this.state.multi_select_list}
                                selectToggleIconComponent={<Text></Text>}
                            /> */}


                            <View style={{ marginRight: 5, padding: 3, marginBottom: 10, marginTop: 5 }}>
                                <TouchableOpacity
                                    onPress={this._addTab} >
                                    <Text style={{ color: '#167f16', fontSize: 18 }}>
                                        +Item
                                    </Text>
                                </TouchableOpacity>
                            </View>

                        </View>
                    }

                </ScrollView>

                {
                    this.state.customer !== '' && this.state.status.showButton ?
                        <View style={{ flexDirection: 'row' }}>
                            <TextInput
                                value={this.state.amount_to_be_collected}
                                keyboardType='numeric'
                                onChangeText={
                                    (amount_to_be_collected) => this.setState({ amount_to_be_collected })
                                }

                                placeholder="Amount"
                                style={{
                                    ...styles.formStyle,
                                    height: 38,
                                    marginTop: 0,
                                    width: 125,

                                }}
                            />
                            <SubmitButton
                                _cannotEdit={this._cannotEdit}
                                isDisabled={this.state.status.val}
                                headerText={this.state.headerText}
                                loader={this.state.loader}
                                _apiCall={
                                    from == 'ManagementScreen' ?
                                        this.state.id === ''
                                            ? this._addData
                                            : this._updateData
                                        : this._addData} />
                        </View>
                        : this.state.id ?
                            <Message val="Cannot update Completed or Partial orders." /> : null
                }

                {
                    this.state.customer !== '' && this.state.status.showButton &&
                    <TextInput
                        onChangeText={(remark) => this.setState({ remark })}
                        value={this.state.remark}
                        placeholder="Remark"
                        style={{
                            ...styles.formStyle,
                            height: 38,
                            margin: 0,
                            marginLeft: 7
                            // width: 125,


                        }}
                    />
                }

            </View >
        )
    }
}

const Message = (props) => (
    <Text style={{ textAlign: 'center', fontSize: 16, fontWeight: '500' }}>
        {props.val}
    </Text>
)

const mapDispatchToProps = {
    dispatchOrders
}

const mapStateToProps = (state) => {
    return {
        customers: state.customersForDropdown,
        orders: state.orders,
        searchOrders: state.searchOrders,
        areas: state.areas,
        products: state.productsForDropdown,
        login: state.login
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUpdateOrderScreen);