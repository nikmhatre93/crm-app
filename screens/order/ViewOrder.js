import React from 'react'
import { View, FlatList, } from 'react-native'
import TableView from '../common/TableView'
import { connect } from 'react-redux'
import { getStatus } from '../common/getBadgeStatus'
import moment from 'moment'
import ModalPopup from '../common/ModalPopup'
import { calculatePrice } from '../common/commonFunctions'

// Header for current screen
const headerItem = {
    product: 'Product',
    quantity: 'Quantity',
    subtotal: 'Total',
    price: 'price',
    gst: 'GST',
    header: true,
    _id: 'key'

}


// View order and its suborder
class ViewOrder extends React.Component {

    _generateList = (item) => {


        let price = item.header
            ? item.price
            : calculatePrice(item.subtotal, item.quantity, this.props.products[item.product].GST)

        let name = item.header ? item.product : this.props.products[item.product].name
        let subtotal = item.header ? item.subtotal : `${parseFloat(item.subtotal).toFixed(2)}`
        let gst = item.header ? item.gst : `${this.props.products[item.product].GST}%`

        return <TableView
            key={item._id}
            // colSr={srNo}
            col1={name}
            col2={item.quantity}
            col3={price}

            // colGST={gst}

            col4={subtotal}
            isHeader={item.header ? item.header : false}
        />
    }

    _orderTotal = () => {
        let Total = 0
        this.props.data.forEach(item => {
            Total += parseFloat(item.subtotal)
        })
        return Total
    }


    render() {
        return (
            <View>
                <OrderHeader
                    currentOrder={this.props.currentOrder}
                    _orderTotal={this._orderTotal} />
                <List _generateList={this._generateList} list={[headerItem, ...this.props.data]} />
            </View>
        )
    }
}


const OrderHeader = (props) => {
    let total = parseFloat(props._orderTotal())
    let paid = parseFloat(total - props.currentOrder.total)
    let bal = parseFloat(props.currentOrder.total)

    return (
        <View>
            <TableView
                col1={moment(props.currentOrder.createdAt).format('ll')}
                col2={`Order Id: ${props.currentOrder.order_id}`}
                colStatus={getStatus(props.currentOrder.status)}
            />
            <TableView
                col1={`Paid : ${paid.toFixed(2)}`}
                col2={`Bal : ${bal.toFixed(2)}`}
                col3={`Total: ${total.toFixed(2)}`}

            />
        </View>
    )
}


const mapStateToProps = state => {
    return {
        products: state.productsForDropdown
    }
}

const View_Order = connect(mapStateToProps, null)(ViewOrder)

const List = (props) => (
    <View style={{ margin: 10 }}>
        <FlatList
            keyExtractor={(item) => item._id}
            data={props.list}
            renderItem={({ item }) => props._generateList(item)}

        />
    </View>
)


// ModalPopup

const ViewOrderModal = (props) => {
    return (
        <ModalPopup
            title='Order'
            isVisible={props.orderModalVisible}
            func={props._toggleOrderModal}>

            <View_Order data={props.orderData} currentOrder={props.currentOrder} />
        </ModalPopup>
    )
}


export default ViewOrderModal


{/* <Modal
isVisible={props.orderModalVisible}
avoidKeyboard={true}>
<View style={{ ...styles.screenBackground, borderRadius: 5, flex: 1 }}>
<View
style={{
flexDirection: "row",
justifyContent: "space-between",
alignItems: "center",
padding: 10,
borderBottomWidth: 1,
borderBottomColor: "#b0b0b0",

}}>
<Text
style={{
fontSize: 20,
color: "black",
// fontFamily: "Montserrat-SemiBold"
}}>
Order
</Text>
<TouchableOpacity
onPress={() => props._toggleOrderModal(false)}
style={{
paddingHorizontal: 15,
paddingVertical: 2,

}}>
<Text>
Close
</Text>
</TouchableOpacity>
</View>
<ScrollView style={{ padding: 5, flex: 1 }}>
<View_Order data={props.orderData} currentOrder={props.currentOrder} />
</ScrollView>
</View>
</Modal> */}