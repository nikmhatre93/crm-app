import React from 'react'
import ManagementScreen from '../common/ManagementScreen'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { getOrders, OrderSearchList } from '../../redux/actions/ordersActions'
import { LogoutButton } from '../AuthScreen'

// Headers for orders screen
export const headerItem = {
    order_id: 'Id',
    customer: { shop_name: 'Shop' },
    total: 'Balance',
    status: 'Status',
    createdAt: ''
}


class OrderScreen extends React.Component {

    componentDidMount = () => {
        this.props.navigation.setParams({
            name: this.props.login.user.username
        })
    }

    static navigationOptions = ({ navigation }) => {
        const name = navigation.state.params ? navigation.state.params.name : ''
        return {
            headerTitle: <Header name='Orders' icon={true} />,
            headerRight: <LogoutButton navigation={navigation} name={name} />
        }
    };

    render() {

        return <ManagementScreen
            buttonText="Place a new order"
            type="order"
            data={this.props.orders}
            searchData={this.props.searchOrders}
            filterUrl='filter_orders'
            headerItem={headerItem}
            screen="AddEditOrder"
            navigation={this.props.navigation}
            getPaginateData={this.props.getOrders}
            searchFunc={this.props.OrderSearchList}
        />
    }

}

const mapStateToProps = (state) => {
    return {
        orders: state.orders,
        searchOrders: state.searchOrders,
        login: state.login
    }
}

mapDispatchToProps = {
    getOrders,
    OrderSearchList
}

export default connect(mapStateToProps, mapDispatchToProps)(OrderScreen);