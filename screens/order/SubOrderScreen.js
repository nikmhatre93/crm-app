import React from 'react'
import { View, TextInput, FlatList, Text, TouchableOpacity, } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { connect } from 'react-redux'
import lodash from 'lodash'
import styles, { dropdownStyle } from '../common/stylesheet'
import SectionedMultiSelect from '../../customized-sectioned-multi-select/sectioned-multi-select';
// import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import Icon3 from 'react-native-vector-icons/MaterialIcons';
import { defaultSubTotal } from './AddEditOrderScreen'
import { does_exist, titleCase } from '../common/commonFunctions'


const bgColors = {
    main: 'white',
    other: '#e6e6e6c2'
}

var _items = [
    {
        id: 1,
        name: '0.100',
    },
    {
        id: 2,
        name: '0.250',
    },
    {
        id: 3,
        name: '0.500',
    },
    {
        id: 4,
        name: '1',
    },
    {
        id: 5,
        name: '1.25',
    },
    {
        id: 6,
        name: '2',
    },
    {
        id: 7,
        name: '2.5',
    },
    {
        id: 8,
        name: '5',
    },
    {
        id: 9,
        name: '10',
    },
    {
        id: 10,
        name: '30',
    },
    {
        id: 11,
        name: '50',
    },
];


class SubOrderScreen extends React.Component {

    _refs_price = {}
    _refs_qty = {}


    // use this ref for focus and blur event
    create_price_refs = (ref, key) => {
        this._refs_price[key] = ref
    }

    // use this ref for focus and blur event
    create_qty_refs = (ref, key) => {
        this._refs_qty[key] = ref
    }

    onSelectedItemsChange = (key) => {
        let ref = this._refs_qty[key]
        ref.focus()
    }

    onSubmitQtyEditing = (key) => {
        let ref = this._refs_price[key]
        ref.focus()
    }

    showHideOptions = (val) => {
        this.setState({ options: val })
    }

    _generateTab = (item, index, len) => {

        let scrollToIndexProp = {
            scrollToIndexProp: item.selectedIndex
        }

        // console.log(this.props.lastIndex, this.props.lastKey, index)
        if (this.props.lastKey == index + 1) {
            // console.log(this.props.lastIndex)
            scrollToIndexProp['scrollToIndexProp'] = this.props.lastIndex
        }


        let d = does_exist(this.props.products, item.product)

        // let _s = d.val != '' &&
        //     item.price.toString() != "" &&
        //     item.quantity != "" ? { borderBottomColor: 'green' } : { borderBottomColor: 'red' }


        let no_price_validation = false
        let price_validation = false

        // check if price validation is disabled
        if (this.props._min_price_validation) {
            no_price_validation = !item.min_price_error
            price_validation = item.price.toString() != ""
        } else {
            no_price_validation = true
            price_validation = true
        }

        // console.log(item)

        let _s = {}
        if (d.val != '' &&
            price_validation &&
            item.quantity != "" && no_price_validation) {
            _s = { borderBottomColor: 'green' }
        } else {
            _s = { borderBottomColor: 'red' }
        }



        let style_guide = index == len - 1 ? {} : { ..._s, borderBottomWidth: 1 }

        return (

            <View
                style={{
                    margin: 5,
                    paddingHorizontal: 1,
                    paddingVertical: 5,

                    borderRadius: 4,
                    borderWidth: 0.5,
                    borderColor: '#b5b5b5',
                    backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main,
                    flex: 1,
                    ...style_guide
                }}>

                <View style={{ flex: 1, flexDirection: 'row', }}>

                    <View style={{ flex: 1 }}>
                        <View style={{ marginTop: 14 }}>
                            <Text style={{ textAlign: 'center', fontSize: 14 }}>
                                {index + 1}
                            </Text>
                        </View>
                    </View>

                    <View style={{ flex: 4 }}>
                        <View style={{
                            ...styles.formStyle,
                            position: 'relative',
                            margin: 3,
                            backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main,
                        }}>
                            <Text style={[
                                styles.labelStyle,
                                { top: -4, left: 3 },
                                { backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main, }
                            ]}>Product</Text>

                            <SectionedMultiSelect
                                styles={{
                                    ...dropdownStyle
                                }}
                                items={lodash.values(this.props.products)}
                                uniqueKey='_id'
                                showDropDowns={true}
                                single={true}
                                showCancelButton={false}
                                hideConfirm={true}
                                renderSelectText={() => d.val != '' ? titleCase(d.name) : "Select Product"}
                                showChips={false}
                                confirmText="Done"
                                hideSearch={false}
                                selectedIconComponent={<Icon3 name="check-circle" size={17} style={{ color: "green" }} />}
                                modalAnimationType="slide"
                                onSelectedItemsChange={(itemValue) => {
                                    this.props._setProduct(
                                        item.key,
                                        itemValue,
                                        this.props.products[itemValue[0]],
                                        this.props.products[itemValue[0]].index
                                    )
                                    this.onSelectedItemsChange(item.key)
                                }}
                                searchPlaceholderText='Search products...'
                                selectedItems={[d.val]}
                                selectToggleIconComponent={<Text></Text>}
                                {...scrollToIndexProp}
                            />

                        </View>
                    </View>

                    <View style={{ flex: 2 }}>
                        <View style={{ position: 'relative' }}>
                            <Text style={{
                                ...styles.labelStyle,
                                top: -2,
                                backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main,
                            }}>Qty kg</Text>

                            <TextInput
                                placeholder="0"
                                style={{
                                    ...styles.formStyle,
                                    margin: 3,
                                    backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main,
                                }}

                                onFocus={() => {
                                    this.props._showOptions(item.key, true)
                                }}

                                onBlur={() => {
                                    this.props._showOptions(item.key, false)
                                }}

                                keyboardType='numeric'
                                ref={(input) => this.create_qty_refs(input, item.key)}

                                onSubmitEditing={() => { this.onSubmitQtyEditing(item.key) }}

                                value={item.quantity == null ? '0' : item.quantity.toString()}
                                onChangeText={(text) => this.props._setQuntity(item.key, text)} />
                        </View>

                    </View>

                    <View style={{ flex: 2 }}>

                        <View style={{ position: 'relative' }}>
                            <Text style={{
                                ...styles.labelStyle,
                                top: -2,
                                backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main,
                            }}>Price</Text>
                            <TextInput
                                placeholder="0"
                                style={{
                                    ...styles.formStyle,
                                    margin: 3,
                                    backgroundColor: this.props.lastKey == item.key ? bgColors.other : bgColors.main,
                                }}
                                keyboardType='numeric'

                                onBlur={() => {
                                    this.props._min_price_error(item.key)
                                }}

                                ref={(input) => this.create_price_refs(input, item.key)}

                                value={item.price.toString()}
                                onChangeText={(text) => this.props._setProductPrice(item.key, text, item.min_price)} />


                        </View>
                    </View>



                    <View style={{ flex: 3 }}>
                        <View style={{ marginTop: 14 }}>
                            <Text style={{ textAlign: 'center', fontWeight: 'bold', fontSize: 12 }}>
                                ₹ {isNaN(item.subtotal) ? defaultSubTotal : parseFloat(item.subtotal).toFixed(2)}
                            </Text>
                        </View>
                    </View>


                    {
                        Object.keys(this.props.data).length > 2 && this.props.lastKey != item.key &&
                        <View style={{ flex: 1, marginTop: 12, marginRight: 0 }}>
                            <TouchableOpacity
                                onPress={() => this.props._removeTab(item.key, item._id)}>
                                <Icon
                                    style={Object.keys(this.props.data).length == 1
                                        && { marginLeft: 1, marginTop: 1 }}
                                    name="delete"
                                    size={24}
                                    color="#FF0000" />
                            </TouchableOpacity>
                        </View>
                    }

                </View>

                {
                    item.options &&
                    <View style={{ flex: 1 }}>
                        {this.renderFlatList(item.key)}
                    </View>
                }

                {/* {
                    d.val != '' && item.price.toString().trim() == '' &&
                    <Text style={{ color: '#753636', fontSize: 12, textAlign: 'center' }}>
                        Minimum Price for {titleCase(d.name)} is {`${item.min_price}`}/-
                    </Text>
                } */}

                {
                    item.min_price_error &&
                    !(parseFloat(item.price.toString().trim()) >= parseFloat(item.min_price)) &&
                    <Text style={{ color: '#753636', fontSize: 12, textAlign: 'center' }}>
                        {`Price can not be less than ${item.min_price}/-`}
                    </Text>
                }
            </View>

        )
    }

    // componentDidUpdate = () => {
    // this.refs.subOrderFlatList.scrollToEnd({ animated: true })
    // }

    renderFlatList = (key) => {
        return (
            <FlatList
                horizontal={true}
                style={{ ...this.props.itemsContainerStyle }}
                keyboardShouldPersistTaps="always"
                data={_items}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => this.renderItems(item, key)} />
        )
    }


    renderItems = (i, key) => {
        return (
            <TouchableOpacity style={{
                ...styles.formStyle,
                height: 28,
                paddingHorizontal: 5,
                paddingTop: 3,
                borderRadius: 4,
                borderWidth: 0.5,
                borderColor: '#b5b5b5',
                margin: 5,
                backgroundColor: 'white',
                fontSize: 14,
                backgroundColor: this.props.lastKey == key ? bgColors.other : bgColors.main,
            }} onPress={() => {
                this.onItemSelect(i, key);
            }}>
                <Text style={{ ...this.props.itemTextStyle }}>
                    {i.name}
                </Text>
            </TouchableOpacity>
        );
    };

    onItemSelect = (item, key) => {
        this.props._setQuntity(key, item.name)
    }


    render() {

        let len = Object.keys(this.props.data).length

        return (
            <View style={{ flex: 1, flexDirection: 'row', marginTop: 5 }}>

                <FlatList
                    keyboardShouldPersistTaps="always"
                    ref="subOrderFlatList"
                    keyExtractor={(item) => item.key.toString()}
                    data={lodash.values(this.props.data)}
                    renderItem={({ item, index }) => this._generateTab(item, index, len)} />

            </View>
        )
    }
}

mapStateToProps = (state) => {
    return {
        products: state.productsForDropdown
    }
}

export default connect(mapStateToProps, null)(SubOrderScreen)
