import React from 'react'
import { View, TouchableOpacity, Text, FlatList } from 'react-native'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { LogoutButton } from '../AuthScreen'
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment'
import styles from '../common/stylesheet'
import axios from 'axios'
import { base_url } from '../../base_url'
import TableView from '../common/TableView'
import { NavigationEvents } from "react-navigation";
import AreaList from '../common/AreaList'


// Act as check list for orders

class CheckList extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            date: moment(),
            date_visible: false,
            data: [],
            area: ''
        }
    }

    componentDidMount = () => {
        this.props.navigation.setParams({
            name: this.props.login.user.username
        })
    }

    static navigationOptions = ({ navigation }) => {
        const name = navigation.state.params ? navigation.state.params.name : ''
        return {
            headerTitle: <Header name='Check List' icon={true} />,
            headerRight: <LogoutButton navigation={navigation} name={name} />
        }
    };


    showDateTimePicker = () => {
        this.setState({ date_visible: true })
    }


    handleDatePicked = (date) => {
        this.setState({ date, date_visible: false }, () => {
            this.getData()
        })
    }


    getData = () => {
        let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }
        let date = moment(this.state.date)

        axios.post(`${base_url}/orders_by_date`, {
            startDate: date.startOf('day').format(),
            endDate: date.endOf('day').format(),
            area: this.state.area
        }, headers)
            .then(res => {
                // console.log(res.data)
                this.setState({ data: res.data })
            })
            .catch(err => {
                console.log(err)
            })
    }

    hideDateTimePicker = (type) => {
        this.setState({
            date_visible: false,
            date: moment()
        })
    }


    // Mark order done i.e. true or false for today
    update_order = (_id, is_done) => {
        let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }
        axios.post(`${base_url}/is_order_done`, { _id, is_done: !is_done }, headers)
            .then(res => {
                let _data = [...this.state.data]
                let data = _data.map(item => {
                    if (item._id == _id) {
                        return { ...item, is_done: !is_done }
                    }
                    return item
                })
                this.setState({ data })
            })
            .catch(err => {
                console.log(err)
            })
    }

    _onDidFocus = () => {
        this.getData()
    }

    _onAreaChange = (itemValue) => {
        this.setState({ area: itemValue }, () => {
            this.getData()
        })
    }

    render() {

        let _areas = { "": { _id: '', name: 'Show All' }, ...this.props.areas }

        return (
            <View style={{ flex: 1 }}>

                <NavigationEvents
                    onDidFocus={this._onDidFocus}
                />

                <View style={{ flexDirection: 'row', flex: 1, marginTop: 5 }}>
                    <View style={{ flex: 1 }}>
                        <DatePicker
                            state={this.state}

                            hideDateTimePicker={this.hideDateTimePicker}
                            handleDatePicked={this.handleDatePicked}

                            showDateTimePicker={this.showDateTimePicker}
                        />
                    </View>

                    <View style={{ flex: 1 }}>
                        <AreaList
                            data={_areas}
                            value={this.state.area}
                            func={this._onAreaChange}
                        />
                    </View>
                </View>

                <View style={{ margin: 8, marginTop: 45, flex: 15 }}>
                    {
                        this.state.data.length > 0 ?
                            <FlatList
                                ListHeaderComponent={

                                    <TouchableOpacity>
                                        <TableView
                                            colSr="Id"
                                            col1="Shop"
                                            col2="Area"
                                            col3="collect"
                                            col4="Amount"
                                            isHeader={true}
                                        />
                                    </TouchableOpacity>

                                }
                                keyExtractor={(item) => item._id}
                                data={this.state.data}
                                renderItem={({ item }) => (

                                    <TouchableOpacity

                                        onPress={() => {
                                            this.update_order(item._id, item.is_done)
                                        }}
                                    >
                                        <TableView
                                            styleObj={item.is_done ? '#e2ffe2' : 'white'}
                                            colSr={item.order_id}
                                            col1={item.customer.shop_name}
                                            col2={item.area.name}
                                            col3={item.amount_to_be_collected === ""
                                                || item.amount_to_be_collected == null
                                                ? " "
                                                : item.amount_to_be_collected}
                                            col4={`${item.total}`}
                                        />
                                    </TouchableOpacity>

                                )}
                                stickyHeaderIndices={[0]}
                            /> : <NoRecordTemplate />
                    }
                </View>


            </View>
        )

    }
}


const DatePicker = (props) => (
    <View style={{ flex: 1 }}>
        <DateTimePicker
            date={new Date(props.state.date)}
            isVisible={props.state.date_visible}
            onConfirm={props.handleDatePicked}
            onCancel={() => props.hideDateTimePicker()}
        />


        <View style={{ flexDirection: 'row', flex: 1 }}>
            <TouchableOpacity
                style={{ flex: 1, ...styles.formStyle }}
                onPress={props.showDateTimePicker}
            >
                <Text style={[styles.labelStyle, { top: -7, left: 3 }]}>Date</Text>
                <Text
                    style={{ textAlign: 'center', justifyContent: 'center', fontSize: 20, marginVertical: 7 }}
                >
                    {moment(props.state.date).format('DD/MM/YYYY')}</Text>
            </TouchableOpacity>

        </View>
    </View>
)

const mapStateToProps = (state) => {
    return {
        login: state.login,
        areas: state.areas,
    }
}

mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(CheckList);