import React from 'react'
import { View, TextInput, Button, AsyncStorage, Text, Picker, Alert } from 'react-native'
// import { postCall } from '../apicalls/apicalls'
import { base_url } from './../base_url'
import Header from '../screens/common/Header'
import { connect } from 'react-redux'
import { login } from '../redux/actions/authActions'
import { getAreas } from '../redux/actions/areaActions'
import { getCustomers, customersForDropdown } from '../redux/actions/customerActions'
import { getOrders } from '../redux/actions/ordersActions'
import { getProducts, productsForDropdown } from '../redux/actions/productActions'
import styles, { btnColor } from './common/stylesheet'
import axios from 'axios'
import Dialog, { DialogContent, DialogFooter, DialogButton, DialogTitle, SlideAnimation } from 'react-native-popup-dialog';
import AreaList from './common/AreaList'

class Logout extends React.Component {


    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            area: ''
        };
    }

    _logout = () => {
        AsyncStorage.removeItem('Logged-In-User')
        AsyncStorage.removeItem('globalArea')
        this.props.navigation.navigate('Login')
    }

    _generateList = (data) => {
        return data.map(item => (<Picker.Item label={item.name} value={item._id} key={item._id} />))
    }
    componentDidMount = async () => {
        let area = await AsyncStorage.getItem('globalArea');
        if (!area) {
            area = ''
        }
        this.setState({ area })
    }

    _onAreaChange = (area) => {
        this.setState({ area: area[0] })
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <View>
                    <Dialog
                        visible={this.state.modalVisible}
                        onTouchOutside={() => {
                            this.setState({ modalVisible: false });
                        }}
                        dialogTitle={<DialogTitle title="Set Global Area" />}
                        dialogAnimation={new SlideAnimation({
                            slideFrom: 'bottom',
                        })}
                        width={200}
                        footer={
                            <DialogFooter>
                                <DialogButton
                                    textStyle={{ color: 'black', fontSize: 15 }}
                                    text="CANCEL"
                                    onPress={() => this.setModalVisible(false)}
                                />
                                <DialogButton
                                    textStyle={{ color: 'black', fontSize: 15 }}
                                    text="OK"
                                    onPress={async () => {
                                        await AsyncStorage.setItem('globalArea', this.state.area);
                                        this.setModalVisible(false)
                                    }}
                                />
                            </DialogFooter>
                        }>
                        <DialogContent>
                            <View style={{ paddingVertical: 10 }}>
                                <AreaList
                                    value={this.state.area == null ? '' : this.state.area}
                                    func={this._onAreaChange}
                                    data={this.props.areas}
                                />
                            </View>

                        </DialogContent>
                    </Dialog>

                </View>

                <View>
                    <Picker
                        style={{ height: 50, width: 110 }}
                        itemStyle={{ fontSize: 16 }}
                        mode="dropdown"
                        selectedValue={this.props.name}

                        onValueChange={(itemValue) => {
                            if (itemValue == 'Logout') {
                                this._logout()
                            } else if (itemValue == 'Area') {
                                this.setModalVisible(true)
                            }
                        }}>

                        <Picker.Item label={this.props.name} value={this.props.name} />
                        <Picker.Item label="Change Area" value="Area" />
                        <Picker.Item label="Logout" value="Logout" />
                    </Picker>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return { areas: state.areas }
}

export const LogoutButton = connect(mapStateToProps, null)(Logout)


// Login screen
class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            identifier: '', // 'nikhil@techzillaindia.com',
            password: '', // 'tech@123,
        }
    }

    _login = () => {
        axios.post(`${base_url}/auth/local`, {
            identifier: this.state.identifier.trim(),
            password: this.state.password.trim()
        })
            .then(response => {

                let res = response.data
                // Only public role users from strapi are allowed to login in this app.
                if (res.user.role.name == "Public") {
                    this.props.login(res)
                    const { navigate } = this.props.navigation;
                    try {

                        this.props.getProducts(res.jwt, 0)
                        this.props.getCustomers(res.jwt)
                        this.props.getOrders(res.jwt, res.user._id)
                        this.props.getAreas(res.jwt)
                        this.props.productsForDropdown(res.jwt)
                        this.props.customersForDropdown(res.jwt)
                        AsyncStorage.setItem('Logged-In-User', JSON.stringify(res));
                    } catch (error) { }
                    navigate('Orders')
                } else {
                    Alert.alert(
                        'Information',
                        'Your not authorized to access this panel',
                        [

                            {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel',
                            },

                        ],
                        { cancelable: false },
                    );
                }

            })
    }

    render() {
        return (
            <View style={{
                flex: 1, marginHorizontal: 15, marginVertical: 100
            }}>

                <Header name="Login" />

                <View style={{ flex: 8 }}>
                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>Email</Text>
                        <TextInput placeholder="Email"
                            style={styles.formStyle}
                            value={this.state.identifier}
                            onChangeText={(text) => this.setState({ identifier: text })}
                        />
                    </View>

                    <View style={{ position: 'relative' }}>
                        <Text style={styles.labelStyle}>Password</Text>
                        <TextInput placeholder="Password"
                            style={styles.formStyle}
                            value={this.state.password}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({ password: text })}
                        />
                    </View>



                    <View style={{ marginHorizontal: 8, marginVertical: 20 }}>
                        <Button title="Submit"
                            color={btnColor}
                            onPress={this._login}
                        />
                    </View>
                </View>
            </View>
        )
    }
}


mapDispatchToProps = {
    login,
    getProducts,
    productsForDropdown,
    getCustomers,
    customersForDropdown,
    getOrders,
    getAreas
}

export default connect(null, mapDispatchToProps)(LoginScreen)