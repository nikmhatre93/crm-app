import React from 'react'
import { AsyncStorage } from 'react-native'
import { connect } from 'react-redux'
import { login } from '../redux/actions/authActions'
import { getAreas } from '../redux/actions/areaActions'
import { getCustomers, customersForDropdown } from '../redux/actions/customerActions'
import { getOrders } from '../redux/actions/ordersActions'
import { getProducts, productsForDropdown } from '../redux/actions/productActions'

// Check if user is logged in or not from AsyncStorage
// and redirect them to respective screen

class InitScreen extends React.Component {
    constructor(props) {
        super(props)
        this._loggedInCheck();
    }

    _loggedInCheck = async () => {
        const user = await AsyncStorage.getItem('Logged-In-User');
        if (user) {
            parsedUser = JSON.parse(user)
            this.props.login(parsedUser)
            this.props.getProducts(parsedUser.jwt, 0)
            this.props.getCustomers(parsedUser.jwt, 0)
            this.props.getOrders(parsedUser.jwt, parsedUser.user._id, 0)
            this.props.getAreas(parsedUser.jwt)
            this.props.productsForDropdown(parsedUser.jwt)
            this.props.customersForDropdown(parsedUser.jwt)
        }
        this.props.navigation.navigate(user ? 'Orders' : 'Login');
    }

    render() {
        return null
    }
}


mapDispatchToProps = {
    login,
    getProducts,
    productsForDropdown,
    getCustomers,
    customersForDropdown,
    getOrders,
    getAreas,
}

export default connect(null, mapDispatchToProps)(InitScreen)