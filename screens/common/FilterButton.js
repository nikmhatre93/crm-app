import React from 'react'
import { View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import Icon2 from 'react-native-vector-icons/MaterialIcons'


export default FilterButton = (props) => {
    return (
        <View >
            <TouchableOpacity
                onPress={() => props.func()}
                style={{
                    backgroundColor: 'white',
                    elevation: 5,
                    padding: 4.5,
                    borderRadius: 2,
                    alignItems: 'center',

                }}>
                {
                    props.dailyCollection
                        ? <Icon2 name="attach-money" size={25} />
                        : <Icon name="filter" size={25} />
                }

            </TouchableOpacity>
        </View>
    )
}