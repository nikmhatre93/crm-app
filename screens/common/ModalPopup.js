import React from 'react'
import { View, Text, ScrollView, TouchableOpacity } from 'react-native'
import Modal from 'react-native-modal'
import styles from './stylesheet'

/* Base modal pop up for all modal */
const ModalPopup = (props) => {
    return (
        <Modal
            isVisible={props.isVisible}
            avoidKeyboard={true}>
            <View style={{ ...styles.screenBackground, borderRadius: 5, flex: 1 }}>
                <View
                    style={{
                        flexDirection: "row",
                        justifyContent: "space-between",
                        alignItems: "center",
                        padding: 10,
                        borderBottomWidth: 1,
                        borderBottomColor: "#b0b0b0",

                    }}>
                    <Text
                        style={{
                            fontSize: 20,
                            color: "black",
                        }}>
                        {props.title}
                    </Text>
                    <TouchableOpacity
                        onPress={() => props.func(false)}
                        style={{
                            paddingHorizontal: 15,
                            paddingVertical: 2,

                        }}>
                        <Text>
                            Close
                        </Text>
                    </TouchableOpacity>
                </View>
                <ScrollView style={{ padding: 5, flex: 1 }}>
                    {props.children}
                </ScrollView>
            </View>
        </Modal>
    )
}


export default ModalPopup