import React from 'react'
import { Text, View } from 'react-native'
import styles, { dropdownStyle } from './stylesheet'
import Icon from 'react-native-vector-icons/MaterialIcons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

/* Customer Dropdown */

export default class CustomerList extends React.Component {

    _generateCustomerList = () => {
        const id = this.props.id
        const customers = this.props.data

        let customerList = []

        Object.keys(customers).forEach(customer => {

            if (id) {
                customers[customer].name = customers[customer].shop_name
                customerList.push(customers[customer])
            } else {
                if (this.props.area == customers[customer]['area']) {
                    customers[customer].name = customers[customer].shop_name
                    customerList.push(customers[customer])
                }
            }

        })
        return customerList
    }

    render() {

        let d = this.props.data[this.props.value]
        let name = ''
        let val = ''
        if (d) {
            name = d.shop_name
            val = this.props.value
        }

        let _s = this.props.is_selected ? { borderBottomColor: 'green' } : { borderBottomColor: 'red' }
        let style_guide = this.props.guide ? { ..._s, borderBottomWidth: 1 } : {}
        let _flex = this.props.style ? this.props.style : {}
        return (
            <View style={[styles.formStyle, style_guide, _flex]}>
                <Text style={[styles.labelStyle, { top: -7, left: 3 }]}>Customer</Text>
                <SectionedMultiSelect
                    items={this._generateCustomerList(this.props.data)}
                    uniqueKey='_id'
                    styles={{
                        ...dropdownStyle
                    }}
                    showDropDowns={true}
                    single={true}
                    showCancelButton={false}
                    hideConfirm={true}
                    renderSelectText={() => val != '' ? name : "Select Customer"}
                    showChips={false}
                    confirmText="Done"
                    hideSearch={false}
                    selectedIconComponent={<Icon name="check-circle" size={17} style={{ color: "green" }} />}
                    modalAnimationType="slide"
                    onSelectedItemsChange={this.props.func}
                    searchPlaceholderText='Search customers...'
                    selectedItems={[val]}
                    selectToggleIconComponent={<Text></Text>} />
            </View>
        )
    }
}