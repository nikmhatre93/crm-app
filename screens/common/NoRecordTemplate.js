import React from 'react'
import { Text } from 'react-native'

export default NoRecordTemplate = () => (
    <Text style={{ textAlign: "center", margin: 15 }}>
        No records found
    </Text>
)