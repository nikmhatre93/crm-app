
/*
    This screen act as base screen for all the major screens
    Customer
    Order
    Payment
    Product
*/

import React, { Component } from 'react'
import { View, Button, FlatList, TouchableOpacity, } from 'react-native'
import { connect } from 'react-redux'
import lodash from 'lodash'
import TableView from '../common/TableView'
import styles, { btnColor } from '../common/stylesheet'
import { getStatus } from './getBadgeStatus'
import FilterButton from './FilterButton'
import FilterForm from './FilterForm'
import { default_limit } from '../../redux/constant'
import { base_url } from '../../base_url'
import moment from 'moment'
import axios from 'axios'
import NoRecordTemplate from '../common/NoRecordTemplate'
import Dialog, { DialogContent, DialogFooter, DialogButton, DialogTitle, SlideAnimation } from 'react-native-popup-dialog';
import { NavigationEvents } from "react-navigation";
import PaymentCollectionScreen from '../payment/PaymentCollectionScreen'
import ViewPaymentModal from '../payment/ViewPayment'
import { titleCase } from './commonFunctions'


let setTimeoutId = null

class ManagementScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            modalVisible: false,
            filterVisible: false,
            paymentModalVisible: false,
            skip: default_limit,
            status: 'Select',
            searchText: '',
            area: '',
            payType: 'Select',
            dailyCollectionData: [],
            paymentData: [],
            user: {},
            customer: {}
        }
    }

    // generate row for flat list
    _generateList = (item, header, search) => {

        let TempText = null
        let TempTouchableOpacity = null

        // Create new row
        if (this.props.type === 'customer') {

            TempText = <TableView
                col1={item.area.name}
                col2={item.shop_name}
                col3={item.owner_name == '' ? ' ' : item.owner_name}
                col5={item.gst_id == '' ? ' ' : item.gst_id}
                isHeader={header}
            />

        } else if (this.props.type === 'product') {
            TempText = <TableView
                col1={titleCase(item.name)}
                col2={item.GST.toString()}
                isHeader={header}
            />
        } else if (this.props.type === 'order') {

            TempText = <TableView
                colSr={item.order_id}
                col2={item.customer.shop_name}
                col3={item.total == null ? '0' : item.total.toString()}
                col4={item.createdAt == '' ? 'Date' : moment(item.createdAt).format('D/M/YY')}
                colStatus={getStatus(item.status)}
                isHeader={header}
            />

        } else {
            TempText = <TableView
                col1={item.createdAt == "Date" ? "Payment Date" : moment(item.createdAt).format('ll')}
                col2={item['cheque/cash'] == 'cq' ? 'Cheque' : 'Cash'}
                col3={item.total_paid.toString()}
                isHeader={header}
            />
        }


        // assign onPress function for generated row
        if (!header) {
            if (this.props.type === 'order') {
                TempTouchableOpacity = <TouchableOpacity
                    onPress={() => this.props.navigation.navigate(this.props.screen,
                        { id: item._id, isSearch: search, from: 'ManagementScreen' }
                    )}>
                    {TempText}
                </TouchableOpacity>
            }
            else if (this.props.type === 'payment') {
                TempTouchableOpacity = <TouchableOpacity
                    onPress={() => {
                        this._togglePaymentModal(true, item._id)
                    }}>
                    {TempText}
                </TouchableOpacity>
            }
            else {
                TempTouchableOpacity = <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate(this.props.screen, {
                            id: item._id,
                            isSearch: search
                        })
                    }}>
                    {TempText}
                </TouchableOpacity>
            }

        } else {
            TempTouchableOpacity = <View>
                {TempText}
            </View>
        }

        return TempTouchableOpacity
    }


    // show and hide filters
    _toggleFilter = () => {
        this.setState({
            filterVisible: !this.state.filterVisible,
            status: 'Select',
            searchText: '',
            area: '',
            payType: 'Select'
        })
    }


    // trigger pagination call on defined threshold
    _onEndReached = () => {

        if (!this.state.filterVisible) {
            let skip = this.state.skip
            this._fetchRecord(skip)
            skip += default_limit
            this.setState({ skip })
        }
    }


    // pagination api call
    _fetchRecord = (skip) => {
        if (this.props.login) {
            if (this.props.type == 'order' || this.props.type == 'payment') {
                this.props.getPaginateData(this.props.login.jwt, this.props.login.user._id, skip)
            } else {
                this.props.getPaginateData(this.props.login.jwt, skip)
            }
        }
    }


    // get the latest data
    _onDidFocus = () => {
        this._fetchRecord(0)
    }


    // for payment screen to redirect to show daily collection
    _paymentCollectionScreen = () => {
        const user = this.props.login.user._id;

        let gt = moment().startOf('day').format()
        let lt = moment().endOf('day').format()

        axios.post(`${base_url}/todaysCollection`, { lt, gt, user },
            { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
            .then(res => {
                this.setState({ dailyCollectionData: res.data })
                this._toggleModal(true)
            })

        // this.props.navigation.navigate('paymentCollectionScreen', {
        //     login: this.props.login
        // })
    }


    // search api call for order (status --> dropdown change )
    _onStatusChange = (itemValue) => {
        this.setState({ status: itemValue })
        if (itemValue == 'Select' && this.state.searchText == '') {
            this.props.searchFunc({})
        } else {
            axios.post(`${base_url}/filter_orders`, {
                status: itemValue,
                query: this.state.searchText,
                user: { _id: this.props.login.user._id, role: this.props.login.user.role.type },
                startDate: '',
                endDate: ''
            }, { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this.props.searchFunc(lodash.keyBy(res.data, '_id'))
                })
        }

    }


    // search api call for payment (payType --> dropdown change )
    _onPayTypeChange = (itemValue) => {
        this.setState({ payType: itemValue })
        if (itemValue == '' && this.state.searchText == '') {
            this.props.searchFunc({})
        } else {
            axios.post(`${base_url}/filter_payment`, {
                type: itemValue,
                query: this.state.searchText,
                user: { _id: this.props.login.user._id, role: this.props.login.user.role.type },
                startDate: '',
                endDate: ''
            }, { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this.props.searchFunc(lodash.keyBy(res.data, '_id'))
                })
        }

    }

    // search api call for customer (area --> dropdown change )
    _onAreaChange = (area) => {

        this.setState({ area })

        if (area == '' && this.state.searchText == '') {
            this.props.searchFunc({})
        } else {
            axios.post(`${base_url}/filter_customers`, {
                area,
                query: this.state.searchText,
                user: { _id: this.props.login.user._id, role: this.props.login.user.role.type }
            }, { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this.props.searchFunc(lodash.keyBy(res.data, '_id'))
                })
        }
    }


    // search call for all (text change)
    _onFilterChange = (text) => {
        this.setState({ searchText: text })

        // throttle of 500ms for reducing search api call on text change
        if (setTimeoutId != null) {
            clearInterval(setTimeoutId)
        }

        setTimeoutId = setTimeout(() => {
            this.searchApiCall(text)
        }, 500)

    }


    // search api
    searchApiCall = (text) => {
        let url = `${base_url}/${this.props.filterUrl}`
        let data = {
            allow: true, query: text,
            type: this.state.payType,
            user: {
                _id: this.props.login.user._id,
                role: this.props.login.user.role.type
            },
            startDate: '',
            endDate: ''
        }

        if (this.props.type == 'order') {
            data.status = this.state.status
            if (text == '' && this.state.status == 'Select') {
                data.allow = false
            }
        } else if (this.props.type == 'customer') {
            data.area = this.state.area
            if (text == '' && this.state.area == '') {
                data.allow = false
            }
        } else {
            if (text == '') {
                data.allow = false
            }
        }


        if (data.allow) {
            axios.post(url, data, { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(response => {

                    let res = response.data
                    if (res.length > 0) {
                        this.props.searchFunc(lodash.keyBy(res, '_id'))
                    } else {
                        this.props.searchFunc({})
                    }
                }).catch(err => {
                    console.log(err)
                })
        } else {
            this.props.searchFunc({})
        }
    }

    // Hide and show filters / search option
    _toggleModal = (val) => {
        this.setState({ modalVisible: val })
    }

    // shows the payment modal for payment screen
    _togglePaymentModal = (val, id) => {

        if (id) {
            axios.get(`${base_url}/subOrderByPayment/${id}`,
                { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this.setState({
                        paymentData: res.data.data,
                        customer: res.data.customer,
                        user: res.data.user,
                        paymentModalVisible: val
                    })
                })
        } else {
            this.setState({ paymentModalVisible: val })
        }
    }

    render() {

        let data = {}

        // if search is enabled use data from different state i.e. (searchData) else (data)
        if (this.props.type == 'order') {
            data = this.state.filterVisible && this.state.searchText != "" || this.state.status != 'Select' ? this.props.searchData : this.props.data
        } else if (this.props.type == 'customer') {
            data = this.state.filterVisible && this.state.searchText != "" || this.state.area != '' ? this.props.searchData : this.props.data
        }
        else if (this.props.type == 'payment') {
            data = this.state.filterVisible && this.state.searchText != "" || this.state.payType != 'Select' ? this.props.searchData : this.props.data
        }
        else {
            data = this.state.filterVisible && this.state.searchText != "" ? this.props.searchData : this.props.data
        }


        return (
            <View style={{ flex: 1, padding: 10, ...styles.screenBackground }}>
                <NavigationEvents
                    onDidFocus={this._onDidFocus}
                />

                <DailyCollection
                    modalVisible={this.state.modalVisible}
                    _toggleModal={this._toggleModal}
                    dailyCollectionData={this.state.dailyCollectionData} />


                <ViewPaymentModal
                    paymentModalVisible={this.state.paymentModalVisible}
                    _togglePaymentModal={this._togglePaymentModal}
                    paymentData={this.state.paymentData}
                    customer={this.state.customer}
                />

                {
                    this.state.filterVisible &&
                    <FilterForm
                        val={this.state.searchText}
                        func={this._onFilterChange}
                        type={this.props.type}
                        _onStatusChange={this._onStatusChange}
                        status={this.state.status}
                        _onPayTypeChange={this._onPayTypeChange}
                        payType={this.state.payType}
                        areas={this.props.areas}
                        area={this.state.area}
                        _onAreaChange={this._onAreaChange}
                    />
                }

                <List
                    list={data}
                    search={this.state.filterVisible}
                    _generateList={this._generateList}
                    headerItem={this.props.headerItem}
                    _onEndReached={this._onEndReached} />

                <View style={{ marginTop: 5, marginBottom: 5 }}></View>

                <AddUpdate
                    classProps={this.props} func={this._toggleFilter}
                    _paymentCollectionScreen={this._paymentCollectionScreen} />
            </View>

        );
    }
}


const AddUpdate = (props) => {
    return (
        <View style={{ justifyContent: 'space-between', flexDirection: 'row' }}>

            <View style={{ flex: 5, marginRight: 4, }}>
                <Button
                    color={btnColor}
                    title={props.classProps.buttonText}
                    onPress={() => props.classProps.navigation.navigate(props.classProps.screen)} />
            </View>


            <View style={{ flex: 1, marginLeft: 4 }}>
                <FilterButton
                    func={props.func}
                    type={props.classProps.type} />
            </View>

            {
                props.classProps.type == 'payment' &&
                <View style={{ flex: 1, marginLeft: 4 }}>
                    <FilterButton
                        func={props._paymentCollectionScreen}
                        type={props.classProps.type}
                        dailyCollection={true}
                    />
                </View>
            }
        </View >
    )
}

const List = (props) => {
    return (
        <View style={{ flex: 9 }}>
            {
                Object.keys(props.list).length > 0
                    ? <FlatList
                        ListHeaderComponent={props._generateList(props.headerItem, true)}
                        keyExtractor={(item) => item._id}
                        data={lodash.values(props.list)}
                        renderItem={({ item }) => props._generateList(item, false, props.search)}
                        stickyHeaderIndices={[0]}
                        onEndReached={props._onEndReached}
                        onEndReachedThreshold={0.2}
                    />
                    : <NoRecordTemplate />
            }

        </View>

    )
}


const DailyCollection = (props) => {
    return (
        <Dialog
            visible={props.modalVisible}
            onTouchOutside={() => {
                props._toggleModal(false);
            }}
            dialogTitle={<DialogTitle title="Daily Collection" />}
            dialogAnimation={new SlideAnimation({
                slideFrom: 'bottom',
            })}
            width={300}
            footer={
                <DialogFooter>
                    <DialogButton
                        textStyle={{ color: 'black', fontSize: 15 }}
                        text="CLOSE"
                        onPress={() => props._toggleModal(false)}
                    />
                    <View></View>
                </DialogFooter>
            }>
            <DialogContent>
                <View style={{ paddingTop: 15 }}>
                    <PaymentCollectionScreen data={props.dailyCollectionData} />
                </View>
            </DialogContent>
        </Dialog>
    )
}





const mapStateToProps = (state) => {
    return {
        login: state.login,
        areas: state.areas
    }
}

export default connect(mapStateToProps, null)(ManagementScreen);