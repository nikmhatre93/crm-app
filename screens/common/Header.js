import React from 'react'
import { View, Text, Image } from 'react-native'
import crm from '../../img/crm.png'

const Header = (props) => {
    return (
        <View style={{ flex: 1, paddingLeft: 10, flexDirection: 'row' }}>

            {
                props.icon &&
                <View style={{ flex: 1 }}>
                    <Image source={crm} style={{ width: 28, height: 28 }} />
                </View>
            }

            <View style={{ flex: 8 }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{props.name}</Text>
            </View>

        </View>
    )
}

export default Header;