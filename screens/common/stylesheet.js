// import React from 'react'
import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    formStyle: {
        borderRadius: 4,
        borderWidth: 0.5,
        borderColor: '#b5b5b5',
        margin: 7,
        backgroundColor: 'white',
        height: 45,
        fontSize: 15
    },
    screenBackground: {
        backgroundColor: '#eff0f1'
    },
    labelStyle: {
        position: 'absolute',
        zIndex: 5,
        paddingHorizontal: 2.5,
        backgroundColor: 'white',
        borderColor: 'black',
        borderRadius: 3,
        borderWidth: 0.1,
        fontSize: 11,
        left: 8,
    },
    boxView: {
        alignSelf: 'stretch',
        flexDirection: 'row',
        backgroundColor: 'white',
        borderRadius: 3,
        elevation: 2
    }

});

export const btnColor = 'slategray'
export const pickerHeight = 40

export const dropdownStyle = {
    selectToggle: {
        height: pickerHeight,
        padding: 8,
    }
}