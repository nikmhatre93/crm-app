import React from 'react'
import { View, Text } from 'react-native'
import { badgeStyle } from './getBadgeStatus'

// Act as Base (Row) for all the tabular data shown.

export default TableView = (props) => {
    let backGroundColor = {}
    let textStyle = {}
    let BadgeStyle = badgeStyle(props.colStatus)
    backGroundColor = BadgeStyle.backGroundColor
    textStyle = BadgeStyle.textStyle

    return (
        <View
            style={{
                flex: 1,
                alignSelf: 'stretch',
                flexDirection: 'row',
                paddingTop: 10,
                paddingBottom: 10,
                backgroundColor: props.styleObj ? props.styleObj : 'white',
                marginBottom: props.isHeader ? 8 : 2,
                elevation: props.isHeader ? 2 : 0,
            }}>

            {
                // colSr is for serial number kind of stuff 
                // (colSr Has flex 1 other col* has flex 2)
                props.colSr &&
                <SubView
                    val={props.colSr}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader}
                    flex1={true}
                />

            }

            {
                props.col1 && <SubView
                    val={props.col1}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />
            }

            {
                props.col2 && <SubView
                    val={props.col2}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />
            }

            {
                props.col3 &&
                <SubView val={props.col3}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />
            }

            {
                // colGST also has flex 1
                props.colGST &&
                <SubView val={props.colGST}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader}
                    flex1={true}
                />
            }

            {
                props.col4 && <SubView
                    val={props.col4}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />
            }


            {
                props.col5 &&
                <SubView val={props.col5}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />
            }

            {
                props.colStatus &&
                <SubView
                    status={true}
                    val={
                        <Badge
                            backGroundColor={backGroundColor}
                            textStyle={textStyle}
                            val={props.colStatus}
                        />
                    }
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />

            }


            {
                // colClickable is used for button
                props.colClickable &&
                <Clickable
                    val={props.colClickable}
                    isHeader={props.isHeader ? props.isHeader : props.subHeader} />

            }


        </View>
    )
}


const Clickable = (props) => {
    if (props.isHeader) {
        return <SubView
            isHeader={props.isHeader}
            val={props.val} />
    } else {
        return <ChildView>
            {props.val}
        </ChildView>
    }
}

// SubView wraps it's child's (props) to provide it particular style
const SubView = (props) => {
    textStyle = { textAlign: 'center' }
    if (props.isHeader) {
        textStyle['fontWeight'] = 'bold'
        textStyle['fontSize'] = 16
    }

    if (props.status) {
        return (
            <ChildView>
                {props.val}
            </ChildView>
        )
    }

    return (
        <ChildView flex1={props.flex1}>
            <Text style={{ ...textStyle }}>
                {props.val}
            </Text>
        </ChildView>
    )
}


const ChildView = (props) => {
    return (
        <View style={{ flex: props.flex1 ? 1 : 2, alignSelf: 'stretch', padding: 1 }}>
            {props.children}
        </View>
    )
}


export const Badge = (props) => {
    return (
        <View style={props.backGroundColor}>
            <Text style={{ ...props.textStyle, textAlign: 'center' }}>
                {props.val.toString()}
            </Text>
        </View>
    )
}