
/* Badge style shown on order screen */

export const getStatus = (status) => {
    let s = ''
    status == '00'
        ? s = 'INIT'
        : status == '01'
            ? s = 'PNDG'
            : status == '10'
                ? s = 'CMPLT' :
                status == '11'
                    ? s = 'PRTL' :
                    status == 'Status'
                        ? s = 'Status' : ''

    return s
}


export const badgeStyle = (val) => {
    let backGroundColor = { width: 55, height: 22, borderRadius: 3, marginRight: 8 }
    let textStyle = { fontSize: 12 }

    switch (val) {
        case 'INIT':
            backGroundColor['backgroundColor'] = '#1e90ff'
            backGroundColor['padding'] = 1
            textStyle['color'] = 'white'
            return {
                backGroundColor,
                textStyle
            }
        case 'PNDG':
            backGroundColor['backgroundColor'] = '#ff6347'
            backGroundColor['padding'] = 1
            textStyle['color'] = 'white'
            return {
                backGroundColor,
                textStyle
            }
        case 'CMPLT':
            backGroundColor['backgroundColor'] = '#3cb371'
            backGroundColor['padding'] = 1
            textStyle['color'] = 'white'
            return {
                backGroundColor,
                textStyle
            }
        case 'PRTL':
            backGroundColor['backgroundColor'] = '#ffa500'
            backGroundColor['padding'] = 1
            textStyle['color'] = 'white'
            return {
                backGroundColor,
                textStyle
            }
        default:
            textStyle['fontWeight'] = 'bold'
            textStyle['fontSize'] = 16
            return {
                backGroundColor,
                textStyle
            }
    }
}