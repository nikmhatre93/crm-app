
/* Capitalize each word in string */
export const titleCase = str => {
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
        splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(' ');
}


/* check give key exits in or not */
export const does_exist = (data, value) => {

    let d = data[value]
    let name = ''
    let val = ''
    if (d) {
        name = d.name
        val = Array.isArray(value) ? value[0] : value
    }

    return { name, val }
}


/* Calculate price for give total, quantity and gst */
export const calculatePrice = (subtotal, quantity, gst, ) => {
    let _p = subtotal / quantity
    let _gst = (gst / 100) + 1
    let price = parseFloat(_p / _gst).toFixed(2)
    return price
}