import React from 'react'
import { View, Button, ActivityIndicator } from 'react-native'
import { btnColor } from '../common/stylesheet'

export default SubmitButton = (props) => {
    return (
        <View style={{ flexGrow: 1, alignContent: 'center' }}>

            {
                props.loader &&
                <ActivityIndicator size={25} style={{ marginTop: 3 }} />
            }

            {
                !props.loader &&
                <Button
                    color={btnColor}
                    title={props.headerText}
                    onPress={props._apiCall}
                />
            }

        </View>
    )
}