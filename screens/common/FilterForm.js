import React from 'react'
import { View, TextInput, Picker, Text } from 'react-native'
import styles, { pickerHeight } from './stylesheet'
import AreaList from './AreaList'


/* 
    Filter form 
    Depending on screen different options are shown
    
*/
export default FilterForm = (props) => {
    // console.log(props)
    return (
        <View style={{ height: 60, flexDirection: "row" }}>
            <View style={{ flex: 2, position: 'relative' }}>
                <Text style={styles.labelStyle}>Search</Text>
                <TextInput
                    value={props.val}
                    onChangeText={(text) => props.func(text)}
                    placeholder="Search"
                    style={styles.formStyle}
                />
            </View>

            {
                props.type == 'order' &&
                <View style={{ ...styles.formStyle, width: 125, position: 'relative' }}>
                    <Text style={[styles.labelStyle, { top: -8, left: 3 }]}>Status</Text>
                    <Picker
                        selectedValue={props.status}
                        style={{ height: pickerHeight }}
                        mode="dropdown"
                        onValueChange={(itemValue) => props._onStatusChange(itemValue)}>
                        <Picker.Item key="Select" label="Select" value="Select" />
                        <Picker.Item key="00" label="Initiated" value="00" />
                        <Picker.Item key="01" label="Pending" value="01" />
                        <Picker.Item key="11" label="Partial" value="11" />
                        <Picker.Item key="10" label="Completed" value="10" />
                    </Picker>

                </View>
            }

            {
                props.type == 'customer' &&
                <View style={{ width: 125, position: 'relative' }}>
                    <AreaList
                        data={props.areas}
                        value={props.area}
                        func={props._onAreaChange}
                    />
                </View>
            }


            {
                props.type == 'payment' &&
                <View style={{ ...styles.formStyle, width: 125, position: 'relative' }}>
                    <Text style={[styles.labelStyle, { top: -8, left: 3 }]}>Pay Type</Text>
                    <Picker
                        selectedValue={props.payType}
                        style={{ height: pickerHeight }}
                        mode="dropdown"
                        onValueChange={(itemValue) => props._onPayTypeChange(itemValue)}>
                        <Picker.Item key="" label="Select" value="Select" />
                        <Picker.Item key="cq" label="Cheque" value="cq" />
                        <Picker.Item key="cs" label="Cash" value="cs" />
                    </Picker>

                </View>
            }


        </View>
    )
}