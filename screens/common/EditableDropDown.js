// Not In Use

// import React, { Component } from 'react';
// import {
//     Text,
//     ListView,
//     FlatList,
//     TextInput,
//     View,
//     TouchableOpacity,
//     Keyboard
// } from 'react-native';

// var ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

// export default class SearchableDropDown extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             item: {},
//             listItems: [],
//             focus: false,
//         };
//     };



//     renderFlatList = () => {
//         if (this.state.focus) {
//             return (
//                 <FlatList
//                     horizontal={true}
//                     style={{ ...this.props.itemsContainerStyle }}
//                     keyboardShouldPersistTaps="always"
//                     data={this.state.listItems}
//                     keyExtractor={(item, index) => index.toString()}
//                     renderItem={({ item }) => this.renderItems(item)} />
//             )
//         }
//     }

//     componentDidMount = () => {
//         const listItems = this.props.items;
//         const defaultIndex = this.props.defaultIndex;
//         if (defaultIndex && listItems.length > defaultIndex) {
//             this.setState({
//                 listItems,
//                 item: listItems[defaultIndex]
//             });
//         }
//         else {
//             this.setState({ listItems });
//         }
//     }

//     renderItems = (item) => {
//         return (
//             <TouchableOpacity style={{ ...this.props.itemStyle }} onPress={() => {
//                 this.setState({ item: item, focus: false });
//                 Keyboard.dismiss();
//                 setTimeout(() => {
//                     this.props.onItemSelect(item);
//                 }, 0);
//             }}>
//                 <Text style={{ ...this.props.itemTextStyle }}>{item.name}</Text>
//             </TouchableOpacity>
//         );
//     };

//     renderListType = () => {
//         return this.renderFlatList();
//     }

//     render() {
//         return (
//             <View style={{ ...this.props.containerStyle }}>
//                 <TextInput
//                     keyboardType='numeric'
//                     underlineColorAndroid={this.props.underlineColorAndroid}
//                     onFocus={() => {
//                         this.setState({
//                             focus: true,

//                             listItems: this.props.items
//                         });
//                     }}
//                     onBlur={() => {
//                         this.setState({ focus: false })
//                     }}
//                     ref={(e) => this.input = e}
//                     onChangeText={(text) => {
//                         this.props.onTextChange(text)
//                     }
//                     }
//                     value={this.props.value}
//                     style={{ ...this.props.textInputStyle }}
//                     placeholderTextColor={this.props.placeholderTextColor}
//                     placeholder={this.props.placeholder} />
//                 {this.renderListType()}
//             </View>
//         );
//     };
// }
