import React from 'react'
import { Text, View } from 'react-native'
import styles, { dropdownStyle } from './stylesheet'
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import lodash from 'lodash'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { does_exist } from '../common/commonFunctions'


/* Area Dropdown */

export default class AreaList extends React.Component {

    render() {

        let d = does_exist(this.props.data, this.props.value)

        let _s = this.props.is_selected ? { borderBottomColor: 'green' } : { borderBottomColor: 'red' }
        let style_guide = this.props.guide ? { ..._s, borderBottomWidth: 1 } : {}

        return (
            <View style={[styles.formStyle, style_guide]}>
                <Text style={[styles.labelStyle, { top: -7, left: 3 }]}>Area</Text>
                <SectionedMultiSelect
                    styles={{
                        ...dropdownStyle
                    }}
                    items={lodash.values(this.props.data)}
                    uniqueKey='_id'
                    showDropDowns={true}
                    single={true}
                    showCancelButton={false}
                    hideConfirm={true}
                    renderSelectText={() => d.val != '' ? d.name : "Select Area"}
                    showChips={false}
                    confirmText="Done"
                    hideSearch={false}
                    selectedIconComponent={<Icon name="check-circle" size={17} style={{ color: "green" }} />}
                    modalAnimationType="slide"
                    onSelectedItemsChange={this.props.func}
                    searchPlaceholderText='Search areas...'
                    selectedItems={[d.val]}
                    selectToggleIconComponent={<Text></Text>} />
            </View>
        )
    }
}

