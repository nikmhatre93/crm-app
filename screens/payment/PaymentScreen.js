import React from 'react'
import ManagementScreen from '../common/ManagementScreen'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { LogoutButton } from '../AuthScreen'

import { getPayments, PaymentSearchList } from '../../redux/actions/paymentActions'

// Header's for Payment screen
export const headerItem = {
    "cheque/cash": 'Cheque/Cash',
    total_paid: 'Amount',
    createdAt: 'Date'
}


// Act as base screen for payment
class PaymentScreen extends React.Component {

    componentDidMount = () => {
        this.props.navigation.setParams({
            name: this.props.login.user.username
        })
    }

    static navigationOptions = ({ navigation }) => {
        const name = navigation.state.params ? navigation.state.params.name : ''
        return {
            headerTitle: <Header name='Payments' icon={true} />,
            headerRight: <LogoutButton navigation={navigation} name={name} />
        }
    };



    render() {
        return <ManagementScreen
            buttonText="Make a payment"
            type="payment"
            filterUrl='filter_payment'
            data={this.props.payments}
            searchData={this.props.searchPayments}
            screen="makePayments"
            navigation={this.props.navigation}
            headerItem={headerItem}
            getPaginateData={this.props.getPayments}
            searchFunc={this.props.PaymentSearchList}
        />
    }
}

const mapStateToProps = (state) => {
    return {
        payments: state.payments,
        searchPayments: state.searchPayments,
        login: state.login
    }
}

mapDispatchToProps = {
    getPayments,
    PaymentSearchList
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentScreen)