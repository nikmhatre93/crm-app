import React from 'react'
import { View, Text } from 'react-native'
import styles from '../common/stylesheet'


// show's user collection amount for today for cash and screen

export default class PaymentCollectionScreen extends React.Component {

    render() {
        let total = 0
        let view = []
        this.props.data.forEach(item => {
            total += parseFloat(item.Total_paid)

            view.push(<BoxView
                key={item._id}
                type={item._id == 'cs' ? 'Cash' : 'Cheque'}
                amt={item.Total_paid}
            />)
        })

        return (
            <View >
                <CollectionAmount total={total} />
                {view}
            </View>
        )
    }
}



const BoxView = (props) => (
    <View style={[styles.boxView, { height: 30, marginHorizontal: 10, marginVertical: 2, elevation: 0 }]}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', fontSize: 17 }}>{props.type}</Text>
        </View>
        <View style={{ flex: 1, justifyContent: 'center' }}>
            <Text style={{ textAlign: 'center', fontSize: 17 }}>₹ {parseFloat(props.amt).toFixed(2)}</Text>
        </View>
    </View>
)

const CollectionAmount = (props) => (
    <Text style={{
        textAlign: 'center',
        fontSize: 16,
        margin: 7
    }}>
        Today's Collection: {" "}
        <Text style={{ fontWeight: '500', }}>
            ₹ {parseFloat(props.total).toFixed(2)}
        </Text>
    </Text>
)