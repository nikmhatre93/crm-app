import React from 'react'
import Header from '../common/Header'
import Orders from './Orders'

class OrderList extends React.Component {

    static navigationOptions = {
        headerTitle: <Header name='Order List' icon={false} />
    };

    render() {
        const { navigation } = this.props;
        const data = navigation.getParam('data')
        return (
            <Orders data={data} />
        )
    }
}

export default OrderList