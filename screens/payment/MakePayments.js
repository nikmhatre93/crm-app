import React from 'react'
import { View, Picker, TextInput, Button, Text, Alert, TouchableOpacity, AsyncStorage, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import Header from '../common/Header'
import { base_url } from '../../base_url'
import CustomerList from '../common/CustomerList'
import AreaList from '../common/AreaList'
import styles, { btnColor, pickerHeight } from '../common/stylesheet'
import axios from 'axios'
import Orders from './Orders'

// Make a payment screen
class MakePayements extends React.Component {

    static navigationOptions = {
        headerTitle: <Header name='Make a payment' />,
    };

    constructor(props) {
        super(props)
        this.state = {
            area: '',
            customer: '',
            amount: '',
            pay_type: 'Select',
            total: 0,
            showOrders: false,
            orderList: [],
            noData: false
        }
    }


    componentDidMount = async () => {
        const { navigation } = this.props;
        const customer = navigation.getParam('customer');

        if (customer) {
            this.setState({
                area: customer.area._id,
            })
            this._setCustomer(customer._id)
        } else {
            let area = await AsyncStorage.getItem('globalArea')
            if (!area) {
                area = ''
            }
            this.setState({ area })
        }

    }

    _pay = () => {
        // check amount is more than entered amount or not
        if (parseFloat(this.state.amount) > parseFloat(this.state.total)) {
            Alert.alert(
                'Warning',
                `Amount cannot be more than ${this.state.total}`,
                [
                    {
                        text: 'OK', onPress: () => { }
                    },
                ],
                { cancelable: false },
            );
        } else {
            axios.post(`${base_url}/payments/payorders`,
                { ...this.state, user: this.props.login.user._id, paymentList: { data: [] } },
                { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } })
                .then(res => {
                    this._toast('Payment successful.', true)

                })
                .catch(error => this._toast('Something went wrong, Please try again later.', false))
        }

    }

    _toast = (val, redirect) => {

        ToastAndroid.show(
            val,
            ToastAndroid.SHORT,
            ToastAndroid.CENTER,
        );

        if (redirect) {
            this.props.navigation.goBack()
        }

    }

    /* Order By Customer */
    _getTotalCustomerPay = (itemValue) => {
        let user = this.props.login.user
        let url = `${base_url}/ordersByCustomer`
        let headers = { headers: { 'Authorization': `Bearer ${this.props.login.jwt}` } }
        let data = { customer: itemValue, num: 1, user: user._id, role: user.role.type }

        axios.post(url, data, headers)
            .then(response => {
                let res = response.data

                if (res.length > 0) {

                    let total = 0
                    res.forEach(item => {
                        total += parseFloat(item.total)
                    })

                    this.setState({
                        orderList: res,
                        showOrders: true,
                        total
                    })
                } else {
                    this.setState({ noData: true })
                }

            })
    }


    _setCustomer = (itemValue) => {
        this.setState({ customer: itemValue, showOrders: false, noData: false })
        this._getTotalCustomerPay(itemValue)
    }

    _onAreaChange = (itemValue) => {
        this.setState({
            area: itemValue,
            customer: '',
            amount: '',
            pay_type: 'Select',
            total: 0,
            showOrders: false,
            orderList: [],
            noData: false
        })
    }

    _viewMore = () => {
        // console.log(this.props)
        this.props.navigation.navigate('orderList', { data: this.state.orderList })
    }

    render() {
        let orderList = this.state.orderList.length > 4 ? this.state.orderList.slice(0, 4) : this.state.orderList

        return (
            <View behavior="padding" style={{ flex: 1, padding: 5, ...styles.screenBackground }}>

                <View style={{ flex: 10 }}>
                    <AreaList
                        data={this.props.areas}
                        value={this.state.area}
                        func={this._onAreaChange} />

                    {
                        this.state.area !== ''
                        && <CustomerList
                            data={this.props.customers}
                            value={this.state.customer}
                            area={this.state.area}
                            func={this._setCustomer}
                        />
                    }


                    {
                        this.state.customer !== '' &&
                        <View style={{ flexDirection: "row" }}>
                            <View style={[styles.formStyle, { flex: 1, position: 'relative' }]}>
                                <Text style={[styles.labelStyle, { top: -8, left: 3 }]}>Payments Via</Text>
                                <Picker
                                    style={{ height: pickerHeight }}
                                    selectedValue={this.state.pay_type}
                                    onValueChange={(itemValue) => this.setState({ pay_type: itemValue })}>
                                    <Picker.Item key="Select" label="Pay via" value="Select" />
                                    <Picker.Item key="cs" label="Cash" value="cs" />
                                    <Picker.Item key="cq" label="Cheque" value="cq" />
                                </Picker>
                            </View>
                            <View style={{ flex: 1, position: 'relative' }}>
                                <Text style={styles.labelStyle}>Amount</Text>
                                <TextInput placeholder="Amount"
                                    style={styles.formStyle}
                                    keyboardType='numeric'
                                    onChangeText={(text) => this.setState({ amount: text })}
                                    value={this.state.amount} />
                            </View>
                        </View>
                    }



                    {
                        this.state.showOrders &&
                        <Orders data={orderList} />
                    }


                    {
                        this.state.showOrders &&
                        <View style={{
                            flexDirection: 'row',
                            marginBottom: 10,
                            justifyContent: 'center'
                        }}>

                            <View style={{ marginHorizontal: 10, flexDirection: 'column', justifyContent: 'center' }}>
                                <Text style={{ fontSize: 15 }}>
                                    Total Orders: {this.state.orderList.length}
                                </Text>
                            </View>
                            {this.state.showOrders && this.state.orderList.length > 4
                                ?
                                <View style={{ marginHorizontal: 10, width: 100 }}>

                                    <TouchableOpacity
                                        style={{
                                            backgroundColor: 'white',
                                            borderRadius: 2,
                                            borderWidth: 0.5,
                                            borderColor: '#b5b5b5',
                                            paddingHorizontal: 15,
                                            paddingVertical: 7,
                                            elevation: 2

                                        }}
                                        onPress={this._viewMore}>
                                        <Text>View More</Text>
                                    </TouchableOpacity>
                                </View>
                                : null}
                        </View>
                    }


                    {
                        this.state.showOrders &&
                        <View style={{
                            flexDirection: 'row',
                            backgroundColor: 'white',
                            paddingTop: 7,
                            paddingBottom: 7,
                            paddingRight: 5,
                            paddingLeft: 5
                        }}>
                            <View style={{ flex: 3 }}>
                                <Text style={{ padding: 5, fontSize: 17 }}>
                                    Balance Amount:
                                <Text style={{ fontWeight: 'bold' }}>
                                        {" "}{this.state.total}/-
                                </Text>
                                </Text>
                            </View>

                            <View style={{ flex: 1 }}>
                                <Button
                                    color={btnColor}
                                    disabled={
                                        this.state.amount && this.state.pay_type != "Select" ? false : true
                                    }
                                    title="Pay"
                                    onPress={this._pay}
                                />
                            </View>
                        </View>
                    }

                    {
                        this.state.noData && this.state.customer != '' ?
                            <View style={{ flex: 1, justifyContent: 'space-evenly' }}>
                                <Text style={{ fontSize: 18, textAlign: 'center' }}>
                                    No pending payments for {"\n"}
                                    <Text style={{ fontWeight: 'bold' }}>
                                        {this.props.customers[this.state.customer].owner_name}{" "}
                                        {this.props.customers[this.state.customer].shop_name}
                                    </Text>
                                </Text>
                            </View> : null
                    }
                </View>
            </View >
        )
    }
}

mapStateToProps = (state) => {
    return {
        areas: state.areas,
        customers: state.customersForDropdown,
        orders: state.orders,
        login: state.login
    }
}

export default connect(mapStateToProps, null)(MakePayements)