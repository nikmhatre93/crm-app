import React from 'react'
import { View, FlatList, ScrollView } from 'react-native'
import TableView from '../common/TableView'
import { getStatus } from '../common/getBadgeStatus'
import moment from 'moment'
import Customer from '../customer/Customer'
// import Modal from 'react-native-modal'
import ModalPopup from '../common/ModalPopup'
import { calculatePrice } from '../common/commonFunctions'

// Header for current screen
const headerItem = {
    product: 'Product',
    quantity: 'Quantity',
    subtotal: 'Total',
    price: 'Price',
    gst: 'GST',
    subHeader: true,
    _id: 'key'
}


// Payment modal

class ViewPayment extends React.Component {

    _generateList = (item) => {
        return (
            <View>
                <SubLists
                    order={item._id.order}
                    subOrder={item.subOrder}
                    _generateSubList={this._generateSubList} />
                <View style={{
                    height: 15,
                }}>
                </View>
            </View>
        )
    }

    _generateSubList = (subItem) => {

        let price = subItem.price
            ? subItem.price
            : calculatePrice(subItem.subtotal, subItem.quantity, subItem.gst)
        let gst = subItem.subHeader ? subItem.gst : `${subItem.gst}%`

        return <TableView
            key={subItem._id}
            col1={subItem.product}
            col2={subItem.quantity}
            col3={price}

            colGST={gst}

            col4={subItem.subtotal}
            subHeader={subItem.subHeader ? subItem.subHeader : false}
        />
    }


    render() {
        return (
            <View>
                <Customer customer={this.props.customer} />
                <List _generateList={this._generateList} list={this.props.data} />
            </View>
        )
    }
}




const List = (props) => (
    <View style={{ margin: 10 }}>
        <FlatList
            keyExtractor={(item) => item._id._id}
            data={props.list}
            renderItem={({ item }) => props._generateList(item)}

        />
    </View>
)

const SubLists = (props) => (

    <View>
        <TableView
            colSr={`Id: ${props.order.order_id}`}
            col1={moment(props.order.createdAt).format('ll')}
            colStatus={getStatus(props.order.status)}
        />

        {
            [headerItem, ...props.subOrder].map((item) => props._generateSubList(item))
        }

    </View>

)


const ViewPaymentModal = (props) => {
    return (
        <ModalPopup
            title='Payment'
            isVisible={props.paymentModalVisible}
            func={props._togglePaymentModal}>
            <ViewPayment customer={props.customer} data={props.paymentData} />
        </ModalPopup>
    )
}

export default ViewPaymentModal
