import React from 'react'
import { FlatList } from 'react-native'
import TableView from '../common/TableView'
// import Header from '../common/Header'
import { getStatus } from '../common/getBadgeStatus'

// Header for current screen
const headerItem = {
    customer: {
        shop_name: 'Shop',
    },
    status: 'Status',
    total: 'Balance',
    order_id: 'Order Id'
}

// Order List
class Orders extends React.Component {

    _generateOrderList = (item, header) => {
        return (
            <TableView
                col1={item.order_id}
                col2={item.customer.shop_name.toString()}
                col3={item.total.toString()}

                colStatus={getStatus(item.status)}
                isHeader={header} />
        )
    }

    render() {

        return (
            <FlatList
                ListHeaderComponent={this._generateOrderList(headerItem, true)}
                style={{ marginTop: 15, marginBottom: 8 }}
                keyExtractor={(item) => item._id.toString()}
                data={this.props.data}
                renderItem={({ item }) => this._generateOrderList(item)}
                stickyHeaderIndices={[0]} />
        )
    }
}

export default Orders