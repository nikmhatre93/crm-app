import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
import rootReducer from './reducers/rootReducer'

export const default_limit = 15
export const findCallLimit = 10000000000

let store = createStore(rootReducer, {
    products: {},
    orders: {},
    customers: {},
    areas: {},
    login: {}
}, applyMiddleware(thunk))

export default store;