import { actions } from '../actions/actions'


// Order reducer
export const orders = (state = {}, action) => {
    switch (action.type) {
        case actions.get_orders_list:
            return Object.assign({}, state, {
                ...state,
                ...action.payload
            })
        case actions.update_orders_list:
            return Object.assign({}, state, {
                ...state.orders,
                [action.payload._id]: action.payload
            })

        case actions.add_order:
            return Object.assign({ [action.payload._id]: action.payload }, state)
        default:
            return state
    }
}


export const searchOrders = (state = {}, action) => {
    switch (action.type) {
        case actions.search_orders_list:
            return action.payload
        default:
            return state
    }
}