import { actions } from '../actions/actions'

// Area reducer
export const areas = (state = {}, action) => {
    switch (action.type) {
        case actions.get_areas_list:
            return action.payload
        case actions.update_areas_list:
            return Object.assign({}, state, {
                ...state.areas,
                [action.payload._id]: action.payload
            })
        default:
            return state
    }
}