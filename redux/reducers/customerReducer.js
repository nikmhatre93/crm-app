import { actions } from '../actions/actions'


//Customer reducer
export const customers = (state = {}, action) => {
    switch (action.type) {
        case actions.get_customers_list:
            return Object.assign({}, state, {
                ...state,
                ...action.payload
            })
        case actions.update_customers_list:
            return Object.assign({}, state, {
                ...state.customers,
                [action.payload._id]: action.payload
            })
        case actions.add_customer:
            return Object.assign({ [action.payload._id]: action.payload }, state)
        default:
            return state
    }
}


export const searchCustomers = (state = {}, action) => {
    switch (action.type) {
        case actions.search_customers_list:
            return action.payload
        default:
            return state
    }
}


export const customersForDropdown = (state = {}, action) => {
    switch (action.type) {
        case actions.customer_dropdown:
            return action.payload
        default:
            return state
    }
}