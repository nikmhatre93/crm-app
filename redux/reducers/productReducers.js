import { actions } from '../actions/actions'


// Product reducer
export const products = (state = {}, action) => {
    switch (action.type) {
        case actions.get_product_list:
            return Object.assign({}, state, {
                ...state,
                ...action.payload
            })
        case actions.update_product_list:
            return Object.assign({}, state, {
                ...state.products,
                [action.payload._id]: action.payload
            })
        case actions.add_product:
            return Object.assign({ [action.payload._id]: action.payload }, state)
        default:
            return state
    }
}



export const searchProducts = (state = {}, action) => {
    switch (action.type) {
        case actions.search_products_list:
            return action.payload
        default:
            return state
    }
}


export const productsForDropdown = (state = {}, action) => {
    switch (action.type) {
        case actions.product_dropdown:
            return action.payload
        default:
            return state
    }
}