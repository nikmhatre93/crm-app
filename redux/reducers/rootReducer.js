import { combineReducers } from 'redux'
import { areas } from './areaReducer'
import { customers, searchCustomers, customersForDropdown } from './customerReducer'
import { orders, searchOrders } from './orderReducer'
import { products, searchProducts, productsForDropdown } from './productReducers'
import { payments, searchPayments } from './paymentReducer'
import { login } from './authReducer'


// Root Reducer
const rootReducer = combineReducers({
    products,
    searchProducts,
    productsForDropdown,

    orders,
    searchOrders,

    customers,
    searchCustomers,
    customersForDropdown,

    payments,
    searchPayments,

    areas,
    login
})


export default rootReducer