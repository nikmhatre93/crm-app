import { actions } from '../actions/actions'


// Area reducer
export const login = (state = {}, action) => {
    switch (action.type) {
        case actions.login:
            return action.payload
        default:
            return state
    }
}