import { actions } from '../actions/actions'


//Payment reducer
export const payments = (state = {}, action) => {
    switch (action.type) {
        case actions.get_payment_list:
            return Object.assign({
                ...action.payload,
                ...state,
            }
                , state)

        case actions.add_payment:
            return Object.assign({ [action.payload._id]: action.payload }, state)
        default:
            return state
    }
}


export const searchPayments = (state = {}, action) => {
    switch (action.type) {
        case actions.search_payment_list:
            return action.payload
        default:
            return state
    }
}
