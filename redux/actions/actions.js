// List of redux actions

export const actions = {
    login: 'login',

    get_product_list: 'get_product_list',
    update_product_list: 'update_product_list',
    search_products_list: 'search_products_list',
    product_dropdown: 'product_dropdown',
    add_product: 'add_product',

    get_customers_list: ' get_customers_list',
    update_customers_list: 'update_customers_list',
    search_customers_list: 'search_customers_list',
    customer_dropdown: 'customer_dropdown',
    add_customer: 'add_customer',

    get_orders_list: ' get_orders_list',
    update_orders_list: 'update_orders_list',
    search_orders_list: 'search_orders_list',
    add_order: 'add_order',

    get_areas_list: ' get_areas_list',
    update_areas_list: 'update_areas_list',

    get_payment_list: 'get_payment_list',
    search_payment_list: 'search_payment_list',
    add_payment: 'add_payment'

}