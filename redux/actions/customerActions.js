import { default_limit } from '../constant'
import { base_url } from '../../base_url'
import lodash from 'lodash'
import { actions } from '../actions/actions'
import axios from 'axios'

/* Convert array of objects to object of objects */
const normalization = (inputData) => {
    let data = {
        customers: {},
        areas: {}
    }

    inputData.forEach(customer => {
        // data.areas[customer.area._id] = customer.area
        // customer.area = customer.area._id
        data.customers[customer._id] = customer
    });

    return data
}

/* 
    Get customer's (Pagination). 
    On each call fetch 15 (default_limit) records skipping the number of records mentioned in _start.
*/
export const getCustomers = (token, _start) => {
    return dispatch => {
        let headers = { headers: { 'Authorization': `Bearer ${token}` } }
        let url = `${base_url}/customers?_start=${_start}&_limit=${default_limit}&_sort=_id:DESC&isRetailer=${false}&isDeleted=${false}`

        axios.get(url, headers)
            .then(res => {
                if (res.data.length > 0) {
                    let data = normalization(res.data)
                    dispatch({
                        type: actions.get_customers_list,
                        payload: data.customers
                    })
                }

            })
            .catch(err => { })

    }
}

/* Search result list */
export const CustomerSearchList = (payload) => {
    // console.log(payload)
    return dispatch => {
        dispatch({
            type: actions.search_customers_list,
            payload
        })
    }
}

export const dispatchCustomers = (payload, type) => {
    return dispatch => {
        dispatch({
            type,
            payload
        })
    }
}


/* 
    Due to pagination we do get all the customer, 
    so we have to fetch them separately 
*/
export const customersForDropdown = (token) => {
    return dispatch => {
        axios.get(`${base_url}/customer_dropdown`, { headers: { 'Authorization': `Bearer ${token}` } })
            .then(res => {
                dispatch({
                    type: actions.customer_dropdown,
                    payload: lodash.keyBy(res.data, '_id')
                })
            })
            .catch(err => { })
    }
}
