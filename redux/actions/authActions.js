import { actions } from '../actions/actions'

/* Logged in user's information */
export const login = (payload) => {
    return dispatch => {
        dispatch({
            type: actions.login,
            payload
        })
    }
}