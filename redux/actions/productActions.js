import { default_limit } from '../constant'
import { base_url } from '../../base_url'
import lodash from 'lodash'
import { actions } from '../actions/actions'
import axios from 'axios'
import { titleCase } from '../../screens/common/commonFunctions'

export const getProducts = (token, _start = 0) => {
    return dispatch => {

        axios.get(
            `${base_url}/products?_start=${_start}&_limit=${default_limit}&_sort=_id:DESC&isDeleted=${false}`,
            { headers: { 'Authorization': `Bearer ${token}` } }
        )
            .then(res => {
                if (res.data.length > 0) {
                    dispatch({
                        type: actions.get_product_list,
                        payload: lodash.keyBy(res.data, '_id')
                    })
                }
            })

    }
}


export const ProductSearchList = (payload) => {
    // console.log(payload)
    return dispatch => {
        dispatch({
            type: actions.search_products_list,
            payload
        })
    }
}

export const dispatchProducts = (payload, type) => {
    return dispatch => {
        dispatch({
            type,
            payload
        })
    }
}


export const productsForDropdown = (token) => {
    return dispatch => {
        axios.get(`${base_url}/product_dropdown`, { headers: { 'Authorization': `Bearer ${token}` } })
            .then(res => {

                let d = {}

                res.data.forEach((item, index) => {
                    d[item._id] = { ...item, name: titleCase(item.name), index }
                })


                dispatch({
                    type: actions.product_dropdown,
                    payload: d
                })
            })
    }
}