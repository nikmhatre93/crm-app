import { default_limit } from '../constant'
import { base_url } from '../../base_url'
import lodash from 'lodash'
import { actions } from '../actions/actions'
import axios from 'axios'


/* 
    Get All the payment  
    On each call fetch 15 (default_limit) records skipping the number of records mentioned in _start.
*/

export const getPayments = (token, userId, skip) => {
    return dispatch => {
        let url = `${base_url}/paymentsByUser`
        let headers = { headers: { 'Authorization': `Bearer ${token}` } }
        let d = {
            skip,
            limit: default_limit,
            user: userId,
            sort: {
                column: '_id',
                type: 'DESC'
            }
        }


        axios.post(url, d, headers)
            .then(res => {
                if (res.data.length > 0) {
                    dispatch({
                        type: actions.get_payment_list,
                        payload: lodash.keyBy(res.data, '_id')
                    })
                }
            })
            .catch(err => { })

    }
}

/* Search result list */
export const PaymentSearchList = (payload) => {
    // console.log(payload)
    return dispatch => {
        dispatch({
            type: actions.search_payment_list,
            payload
        })
    }
}

export const dispatchPayments = (payload, type) => {
    return dispatch => {
        dispatch({
            type,
            payload
        })
    }
}
