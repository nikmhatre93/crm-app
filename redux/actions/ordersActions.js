import { default_limit } from '../constant'
import { base_url } from '../../base_url'
import { actions } from '../actions/actions'
import axios from 'axios'

/* Convert array of objects to object of objects */
const normalization = (inputData) => {
    let data = {
        orders: {},
        // customers: {}
    }

    inputData.forEach(order => {
        // data.customers[order.customer._id] = order.customer
        data.orders[order._id] = order
    });

    return data
}

/*
    Get all the orders (Pagination).
    On each call fetch 15 (default_limit) records skipping the number of records mentioned in _start.
*/
export const getOrders = (token, userId, skip) => {
    return dispatch => {
        let url = `${base_url}/ordersById/${userId}`
        let headers = { headers: { 'Authorization': `Bearer ${token}` } }
        let d = {
            skip,
            limit: default_limit,
            sort: {
                column: '_id',
                type: 'DESC'
            }
        }

        axios.post(url, d, headers)
            .then(res => {
                if (res.data.length > 0) {
                    let data = normalization(res.data)
                    dispatch({
                        type: actions.get_orders_list,
                        payload: data.orders
                    })
                }
            })
            .catch(err => { })
    }
}

/* Search result list */
export const OrderSearchList = (payload) => {
    return dispatch => {
        dispatch({
            type: actions.search_orders_list,
            payload
        })
    }
}

export const dispatchOrders = (payload, type) => {
    return dispatch => {
        dispatch({
            type,
            payload
        })
    }
}