import { base_url } from '../../base_url'
import lodash from 'lodash'
import { actions } from '../actions/actions'
import axios from 'axios'

/* Get list of all areas */
/* lodash.keyBy convert array of objects to object of objects */
export const getAreas = (token) => {
    return dispatch => {
        let url = `${base_url}/areas?_sort=name:ASC&name_ne=Other`
        let headers = { headers: { 'Authorization': `Bearer ${token}` } }

        axios.get(url, headers)
            .then(res => {
                dispatch({
                    type: actions.get_areas_list,
                    payload: lodash.keyBy(res.data, '_id')
                })
            })
            .catch(err => { })
    }
}

/*  not in use */
export const dispatchAreas = (payload) => {
    return dispatch => {
        dispatch({
            type: actions.update_areas_list,
            payload
        })
    }
}