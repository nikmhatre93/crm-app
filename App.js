/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React from 'react';
import { Image } from 'react-native'
import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation'
import { Provider } from 'react-redux';
import store from './redux/constant'

// Screens
import CustomerScreen from './screens/customer/CustomerScreen'
import AddEditCustomerScreen from './screens/customer/AddEditCustomerScreen'
import ProductScreen from './screens/product/ProductScreen'
import AddEditProductScreen from './screens/product/AddEditProductScreen'
import LoginScreen from './screens/AuthScreen'
import InitScreen from './screens/InitScreen'
import OrderScreen from './screens/order/OrderScreen'
import AddEditOrderScreen from './screens/order/AddEditOrderScreen'
import PaymentScreen from './screens/payment/PaymentScreen'
import OrderList from './screens/payment/OrderList'
import MakePayments from './screens/payment/MakePayments'
import ViewPayment from './screens/payment/ViewPayment'
import CustomerOrdersHistory from './screens/customer/CustomerOrdersHistory'
import CustomerPaymentHistory from './screens/customer/CustomerPaymentHistory'
import CheckListScreen from './screens/order/CheckList'

// Icons
import ProductIcon from './img/printer-cartridges.png'
import CustomerIcon from './img/networking.png'
import PaymentIcon from './img/money.png'
import OrderIcon from './img/shopping-cart-23.png'
import CheckListIcon from './img/checklist.png'

// Customer screens
const Customers = createStackNavigator({
  manageCustomers: CustomerScreen,
  AddEditCustomer: AddEditCustomerScreen,
  customerOrdersHistory: CustomerOrdersHistory,
  customerPaymentHistory: CustomerPaymentHistory
})

Customers.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


// Product screens
const Products = createStackNavigator({
  manageProducts: ProductScreen,
  AddEditProduct: AddEditProductScreen
})

Products.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


// Order Screens
const Orders = createStackNavigator({
  manageOrders: OrderScreen,
  AddEditOrder: AddEditOrderScreen
})

Orders.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


// Payment Screens
const Payments = createStackNavigator({
  managePayments: PaymentScreen,
  orderList: OrderList,
  makePayments: MakePayments,
  viewPayment: ViewPayment,
})

Payments.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};


// Order Screens
const CheckList = createStackNavigator({
  CheckListScreen
})

Orders.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
  };
};

const BottomTabNavigator = createBottomTabNavigator({
  Orders: {
    screen: Orders,
    navigationOptions: {
      tabBarIcon: () => (
        <Image source={OrderIcon} style={{ width: 22, height: 22 }} />
      )
    }
  },
  Customers: {
    screen: Customers,
    navigationOptions: {
      tabBarIcon: () => (
        <Image source={CustomerIcon} style={{ width: 22, height: 22 }} />
      )
    }
  },
  Products: {
    screen: Products,
    navigationOptions: {
      tabBarIcon: () => (
        <Image source={ProductIcon} style={{ width: 22, height: 22 }} />
      )
    }
  },
  // Payments: {
  //   screen: Payments,
  //   navigationOptions: {
  //     tabBarIcon: () => (
  //       <Image source={PaymentIcon} style={{ width: 22, height: 22 }} />
  //     )
  //   }
  // },
  CheckList: {
    screen: CheckList,
    navigationOptions: {
      tabBarIcon: () => (
        <Image source={CheckListIcon} style={{ width: 22, height: 22 }} />
      )
    }
  }
})

const Auth = createStackNavigator(
  {
    Login: LoginScreen
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);

const AppInit = createStackNavigator(
  {
    Init: InitScreen
  },
  {
    headerMode: 'none',
    navigationOptions: {
      headerVisible: false,
    }
  }
);


const Navigator = createSwitchNavigator({
  Init: AppInit,
  Auth: Auth,
  Nav: BottomTabNavigator
})

export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    )
  }
}